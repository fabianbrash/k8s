## Notes for my CKA exam prep

#### In k8s version 1.19+, we can specify the –replicas option to create a deployment with 4 replicas.

````
kubectl create deployment --image=nginx nginx --replicas=4 --dry-run=client -o yaml > nginx-deployment.yaml

````


```Namespaces```

### Switch permanently to another namespace

````
kubectl config set-context $(kubectl config current-context) --namespace=dev
````

```Namespace Resource quota```

````
---
apiVersion: v1
kind: ResourceQuota
metadata:
  name: dev-ns-quota
  namespace: dev

spec:
  hard:
    pods: "10"
    requests.cpu: "10"
    requests.memory: 2Gi
    limits.cpu: "20"
    limits.memory: 10Gi


````

### Service

#### Create a Service named redis-service of type ClusterIP to expose pod redis on port 6379

````
kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml

(This will automatically use the pod’s labels as selectors)



kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml


kubectl expose pod nginx --type=NodePort --port=80 --name=nginx-service --dry-run=client -o yaml

kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run=client -o yaml

#This will not use the pods labels as selectors


#Create a new pod called custom-nginx using the nginx image and expose it on container port 8080.

kubectl run custom-nginx --image=nginx --port=8080


kubectl run httpd --image=httpd:alpine --port=80 --expose

````

```Selectors```

````
kubectl get pods --selector=env=dev  #get all pods with labels env=dev

kubectl get pods --selector=bu=finance #get all pods with label bu=finance

kubectl get pods --show-labels #show all labels on all pods

kubectl get all --selector=env=prod

kubectl get pod --selector=env=prod,bu=finance,tier=frontend

````

### Master nodes
1. etcd 
2. kube-apiserver
3. Kube Controller Manager 
4. kube-scheduler

### Worker nodes
1. kubelet
2. Kube-proxy
3. Container Runtime(Docker is deprecated)containerd, CRI-O

### ETCD 
````
curl -LO https://github.com/etcd-io/etcd/releases/download/v3.4.16/etcd-v3.4.16-linux-amd64.tar.gz

tar -xzvf etcd-v3.4.16-linux-amd64.tar.gz

./etcdctl

./etcdctl set key1 value1

./etcdctl get key1

kubectl exec etcd-master -n kube-system etcdctl get / --prefix -keys-only
````

### Kube API Server

````
curl -X POST /api/v1/namespaces/default.pods ...[other]
````
1. Authenticate User
2. Validate Request
3. Retrieve data
4. Update ETCD
5. scheduler
6. Kubelet

### Kube-Controller-Manager
#### Controller
1. Watch Status
2. Remediate Situation
3. Node Monitor Period (5s)
4. Node Monitor Grade Period (40s)
5. POD Evention Timeout (5m)

#### Types of Controllers in k8s
1. Node-Controller
2. Replication-Controller
3. Service-Account-Controller
4. Deployment-Controller
5. Namespace-Controller
6. Endpoint-Controller
7. Job-Controller
8. PV-Protection-Controller
9. PV-Binder-Controller

````
/etc/kubernetes/manifests/kube-controller-manager.yaml

cat /etc/systemd/system/kube-controller-manager.service

ps -aux | grep -i kube-controller-manager
````
#### Note the above location /etc/kubernetes/manifests is where the manifests for k8s infra live

#### Kube Scheduler
#### Scheduler decides which pods go on which nodes
1. Filter nodes
2. Rank nodes(scale 0-10)
````
ps -aux | grep kube-scheduler
````

#### Kubelet
##### kubelet is kind of like a captain on a ship
1. Register nodes
2. Create PODs
3. Monitor Node & PODs

####Kubeadm does not deploy kubelet you have to do it manually??(I need to check that)
````
ps -aux | grep kubelet
````
#### Kube Proxy
#### Runs on each nodes in the cluster, looks for a new service so it can forward to backend pods, uses IPTables

#### PODs
#### pods are the smallest object in k8s, pods can have multiple containers in them
#### If you have a multi-container pod they can speak to each other as localhost(localhost:3000, localhost:5000)

````
kubectl run redis --image=redis --dry-run=client -o yaml > podspec.yaml

kubectl get pod my-pod -o yaml

````

#### ReplicaSets
#### Replicaton Controller is replaced by Replica Set

````
apiVersion: v1
kind: ReplicationController
metadata:
  name: myapp-rc
  labels:
    app: myapp
    type: front-end
spec:
  replicas: 3
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
      - name: nginx-rc
        image: nginx
````

````
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: myapp-replicaset
  labels:
    app: myapp
    type: front-end
spec:
  replicas: 3
  selector:
    matchLabels:
      app: myapp
  template:
    metadata:
      name: myapp-pod
      labels:
        app: myapp
        type: front-end
    spec:
      containers:
      - name: nginx-rs
        image: nginx
````
````
kubectl scale -replicas=6 -f myfile.yaml 
````

### Delete pods right away and don't wait

````
kubectl delete pod new-replica-set-x875v --grace-period=0 --force
````

#### Deployments

````
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: www-fb-com
  name: www-fb-com
  namespace: fabianbrash-com
spec:
  replicas: 3
  selector:
    matchLabels:
      app: www-fb-com
  template:
    metadata:
      labels:
        app: www-fb-com
    spec:
      containers:
      - image: myimage
        name: fabianbrashcom
        ports:
        - containerPort: 5000
````

#### We can also create a template from using the below command so we don't have to remember the above

````
kubectl create deploy my-deploy --image=nginx --dry-run=client -o yaml > deploy-template.yaml

cd my-app
kubectl create -f .
````

#### Namespaces 
#### k8s creates 3 namespaces by default
1. kube-system
2. default
3. kube-public

#### Connect to a service in another namespace

````
mysql.connect("db-service.dev.svc.cluster.local")
````

#### The  above would connect to the db-service in the dev namespace

````
kubectl create -f pod-def.yaml --namespace=dev
````

#### The above is the imperative way of doing it, preferred way would be declarative

````
apiVersion: v1
kind: Namespace
metadata:
  name: dev
````

#### Let's change our context

````
kubectl config set-context $(kubectl config current-context) --namespace=dev
````
#### Create a ns with a quota

````
apiVersion: v1
kind: Namespace
metadata:
  name: dev

spec:
  hard:
    pods: "10"
    requests.cpu: "4"
    requests.memory: 5Gi
    limits.cpu: "10"
    limits.memory: 10Gi
````

#### Services

##### Services can expose a deployment, services come in 3 flavors
1. NodePort
2. ClusterIP
3. LoadBalancer

1. TargetPort - port on the pod
2. Port - port on the service
3. NodePort - port on the node itself valid range is 30000 - 32767

````
apiVersion: v1
kind: Service
metadata:
  name: myapp-service

spec:
 type: NodePort
 ports:
  - targetPort: 80
    port: 80
    nodePort: 30008
 selector:
   app: myapp
   type: front-end
````

````
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
````

#### ClusterIP

````
apiVersion: v1
kind: Service
metadata:
  name: back-end

spec:
  type: ClusterIP
  selector:
    app: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80
````

#### Check syntax above


#### LoadBalancer

#### This integrates with your cloud LBs or you can use metallb on prem or NSX

````
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
  type: LoadBalancer
````
#### Declarative vs Imperative

#### Declarative is declaring the end state and let the system figure out how to get there

#### Imperative is giving details instructions

##### Imperative examples

````
kubectl set image deployment nginx nginx=nginx:1.18
kubectl scale deploy nginx --replicas=5
kubectl run nginx --image=nginx
kubectl create deploy nginx --image=nginx
kubectl expose deploy nginx --port 80 type=NodePort
````

#### Declarative example

````
kubectl apply -f myapp.yaml
````

#### More imperative examples

````
kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml

kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml

kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run=client -o yaml

kubectl create deployment nginx --image=nginx --replicas=4

kubectl create deployment nginx --image=nginx--dry-run=client -o yaml > nginx-deployment.yaml

kubectl get pods --show-labels

kubectl expose pod redis --port=6379 --type=ClusterIP --name=redis-service

kubectl run hazelcast --image=hazelcast/hazelcast --labels="app=hazelcast,env=prod"

kubectl run custom-nginx --image=nginx --port=8080

kubectl create deploy redis-deploy --image=redis --replicas=2 -n dev-ns
````
[kubectl cheat sheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

#### Manual Scheduling

##### looks for nodeName property nodeName: node02 in the pod spec
##### also the above can only be done at creating for existing pods you have to use the binding object and send a json request to the api Server

````
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  nodeName: foo-node # schedule pod to specific node
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent

````

#### Labels, Selectors and Annotations

````
kubectl get pods --selector app=App1


apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: App1
  annotations:
    buildversion: 1.34
spec:
  nodeName: foo-node # schedule pod to specific node
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
````

````
kubectl get pod --selector env=prod,bu=finance,tier=frontend


kubectl get pods -l environment=production,tier=frontend

kubectl get node node01 --show-labels

````


#### Taints and Tolerations

#### Nodes get tainted, pods by default can't tolerate taints, pods get toleratations

````
kubectl taint nodes mynode app=blue:NoSchedule

---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: App1
  annotations:
    buildversion: 1.34
spec:
  nodeName: foo-node # schedule pod to specific node
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
  toleratations:
  - key: "app"
    operator: "Equal"
    value: "blue"
    effect: "NoSchedule"

---
kubectl describe node kubemaster | grep Taint


kubectl taint nodes controlplane node-role.kubernetes.io/master-

kubectl taint nodes --all node-role.kubernetes.io/master-
````

#### Note the above has not space after master and the second command is to remove taint from all nodes


#### Node Selectors

````
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod

spec:
  containers:
  - name: data-processor
    image: data-processor
  nodeSelector:
    size: Large

---

kubectl label nodes node01 size=Large
````

#### Node Affinity

````
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod

spec:
  containers:
  - name: data-processor
    image: data-processor
  
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: size
            operator: In 
            values:
            - Large
            #- Medium

---
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod

spec:
  containers:
  - name: data-processor
    image: data-processor
  
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoreDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: size
            operator: Exists


---
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: red
  name: red
spec:
  replicas: 2
  selector:
    matchLabels:
      app: red
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: red
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: node-role.kubernetes.io/master
                operator: Exists
````

#### Resource Limits 
#### By default 0.5 CPU and 256Mi of RAM by default the limit is 1vCPU and 512 Mi for RAM
#### If a pod tries to use more CPU than it's limit it gets throttled, if it tries to use more memory than it's limit it gets terminated

````
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod

spec:
  containers:
  - name: data-processor
    image: data-processor
    ports:
      - containerPort: 8080
    resources:
      requests:
        memory: "1Gi"
        cpu: 1
      limits:
        memory: "2Gi"
        cpu: 2
        
  
````

## Note on default resource requirements and limits

### In the previous lecture, I said – “When a pod is created the containers are assigned a default CPU request of .5 and memory of 256Mi”. For the POD to pick up those defaults you must have first set those as default values for request and limit by creating a LimitRange in that namespace.

````
apiVersion: v1
kind: LimitRange
metadata:
  name: mem-limit-range
spec:
  limits:
  - default:
      memory: 512Mi
    defaultRequest:
      memory: 256Mi
    type: Container

````

````

apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-limit-range
spec:
  limits:
  - default:
      cpu: 1
    defaultRequest:
      cpu: 0.5
    type: Container

````

[https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/](https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/)

[https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/cpu-default-namespace/](https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/cpu-default-namespace/)

[https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource](https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource)

#### DaemonSets

````
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: elasticsearch
  labels:
    type: fluentd-logging
  namespace: kube-system
  
spec:
  selector:
    matchLabels:
      name: elasticsearch
  template:
    metadata:
      labels:
        name: elasticsearch
    spec:
      containers:
      - name: elasticsearch-pod
        image: k8s.gcr.io/fluentd-elasticsearch:1.20
````

#### Static PODs
#### /etc/kubernetes/manifests you can place pod definition files here to start pods(only for pods nothing else)
#### the path this is used is set on the kubelet.service file as --pod-manifest-path
#### also --config=kubeconfig.yaml inside of that file you can do the below

#### Have a look at the output from

````
systemctl status kubelet

````

#### From the above I saw that the config file was in /var/lib/kubelet/config.yaml & I just cat the file and found where the below was pointing to

````
staticPodPath: /etc/kubernetes/manifests
````

#### We can then use docker ps to see all the pods(Note you will nee to use the containerd tools going forward)
#### You can use this method to deploy control plane components to bootstrap a k8s cluster, kubeadm uses this method
#### kube-schedule ignores both static pods and DaemonSets

#### Location of .service files on Ubuntu 18.04

````
/etc/systemd/system/multi-user.target.wants
````

````
kubectl run static-busybox --image=busybox --dry-run=client -o yaml -- 'sleep 1000' > static-pod.yaml #this causes a crashLoop, because sleep isn't on the path

kubectl run busybox --image=busybox --restart=Never -o yaml --dry-run=client -- /bin/sh -c 'echo hello;sleep 3600'

kubectl run --restart=Never --image=busybox static-busybox --dry-run=client -o yaml --command -- sleep 1000 > /etc/kubernetes/manifests/static-busybox.yaml

````

[command vs args](https://stackoverflow.com/questions/59248318/kubectl-run-command-vs-arguments)

#### Multiple Schedulers
#### k8s can have multiple schedulers at the same time
#### kube-scheduler.service has an option --scheduler-name=default-scheduler
#### my-custom-sscheduler.service --scheduler-name=,y-custom-scheduler
#### kubeadm uses /etc/kubernetes/manifests/kube-scheduler.yaml

````
apiVersion: v1
kind: Pod
metadata:
  name: kube-scheduler
  namespace: kube-system

spec:
  containers:
  - command:
    - kube-scheduler
    - --address=127.0.0.1
    - --kubeconfig=/etc/kubernetes/scheduler.conf
    - --leader-elect=true
    image: k8s.gcr.io/kube-scheduler-amd64:v1.11.3
    name: kube-scheduler
  
---

apiVersion: v1
kind: Pod
metadata:
  name: my-custom-scheduler
  namespace: kube-system

spec:
  containers:
  - command:
    - kube-scheduler
    - --address=127.0.0.1
    - --kubeconfig=/etc/kubernetes/scheduler.conf
    - --leader-elect=true
    - --scheduler-name=my-custom-scheduler
    - --lock-object-name=my-custom-scheduler
    image: k8s.gcr.io/kube-scheduler-amd64:v1.11.3
    name: kube-scheduler


apiVersion: v1
kind: Pod
metadata:
  name: nginx

spec:
  containers:
  - image: nginx
    name: nginx
  schedulerName: my-custom-scheduler

---
kubectl get events

kubectl logs my-custom-scheduler -n kube-system



---
apiVersion: v1
kind: Pod
metadata:
  labels:
    component: my-scheduler
    tier: control-plane
  name: my-scheduler
  namespace: kube-system
spec:
  containers:
  - command:
    - kube-scheduler
    - --authentication-kubeconfig=/etc/kubernetes/scheduler.conf
    - --authorization-kubeconfig=/etc/kubernetes/scheduler.conf
    - --bind-address=127.0.0.1
    - --kubeconfig=/etc/kubernetes/scheduler.conf
    - --leader-elect=false
    - --port=10282
    - --scheduler-name=my-scheduler
    - --secure-port=0
    image: k8s.gcr.io/kube-scheduler:v1.19.0
    imagePullPolicy: IfNotPresent
    livenessProbe:
      failureThreshold: 8
      httpGet:
        host: 127.0.0.1
        path: /healthz
        port: 10282
        scheme: HTTP
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    name: kube-scheduler
    resources:
      requests:
        cpu: 100m
    startupProbe:
      failureThreshold: 24
      httpGet:
        host: 127.0.0.1
        path: /healthz
        port: 10282
        scheme: HTTP
      initialDelaySeconds: 10
      periodSeconds: 10
      timeoutSeconds: 15
    volumeMounts:
    - mountPath: /etc/kubernetes/scheduler.conf
      name: kubeconfig
      readOnly: true
  hostNetwork: true
  priorityClassName: system-node-critical
  volumes:
  - hostPath:
      path: /etc/kubernetes/scheduler.conf
      type: FileOrCreate
    name: kubeconfig
status: {}

````

### Updated custom scheduler YAML

````

---
apiVersion: v1
kind: Pod
metadata:
  labels:
    run: my-scheduler
  name: my-scheduler
  namespace: kube-system
spec:
  serviceAccountName: my-scheduler
  containers:
  - command:
    - /usr/local/bin/kube-scheduler
    - --config=/etc/kubernetes/my-scheduler/my-scheduler-config.yaml
    image: k8s.gcr.io/kube-scheduler:v1.23.0 # changed
    livenessProbe:
      httpGet:
        path: /healthz
        port: 10259
        scheme: HTTPS
      initialDelaySeconds: 15
    name: kube-second-scheduler
    readinessProbe:
      httpGet:
        path: /healthz
        port: 10259
        scheme: HTTPS
    resources:
      requests:
        cpu: '0.1'
    securityContext:
      privileged: false
    volumeMounts:
      - name: config-volume
        mountPath: /etc/kubernetes/my-scheduler
  hostNetwork: false
  hostPID: false
  volumes:
    - name: config-volume
      configMap:
        name: my-scheduler-config

````

```my-scheduler-config.yaml```

````
apiVersion: kubescheduler.config.k8s.io/v1beta2
kind: KubeSchedulerConfiguration
profiles:
  - schedulerName: my-scheduler
leaderElection:
  leaderElect: false

````

#### Logging and Monitoring
#### As of recording in 2018 k8s does not have a monitoring stack but you can use the below
1. Metrics Server
2. Prometheus
3. Grafana
4. Elastic stack
5. Datadog
6. Dynatrace

#### Heapster was replaced by metrics server(in-memory only)
#### kubelet has cAdvisor to send metrics
#### If running minikube run minikube addons enable metrics-server

#### Logs

````
kubectl logs -f event-simulator-pod

kubectl logs -f event-simulator-pod event-simulator

````
#### Note the above is for multi-container pods

#### Application Lifecycle Management

#### Rolling Updates and Rollbacks
#### 2 types of deployment strategies
1. Recreate - destroy everything first, then deploy new ones
2. RollingUpdate - Do in a rolling fashion 1 by one it does not cause an outage this is the default

````
kubectl rollout status mydeployment

kubectl rollout history mydeployment
kubectl set image deployment/mydeploy nginx=nginx:1.9.1

kubectl rollout undo deployment/mydeployment
````

````
apiVersion: apps/v1
kind: Deployment
metadata:
  name: archetype-deployment
  labels:
    app: archetype
spec:
  replicas: 3
  selector:
    matchLabels:
      app: archetype
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: archetype
    spec:
      containers:
      - name: archetype
        image: duffney/archetype:v1
        ports:
        - containerPort: 80
````

#### Docker commands & pod definition files

````
docker run ubuntu sleep 5

CMD["sleep", "5"]

----
FROM ubuntu
ENTRYPOINT ["sleep"]
---
docker run ubuntu-sleeper 10

---
FROM Ubuntu
ENTRYPOINT ["sleep"]

CMD ["5"]
---
docker run ubuntu-sleeper  //by default 5 seconds

---
docker run --entrypoint sleep2.0 ubuntu-sleeper 10

---
apiVersion: v1
kind: Pod
metadata:
  name: my-pod

spec:
  containers:
  - name: ubuntu-sleeper
    image: ubuntu-sleeper
    command: ["sleep2.0"] //this overwrites the ENTRYPOINT field so ENTRYPOINT["sleep"] now ENTRYPOINT["sleep2.0"]
    args: ["10"] //this is overwriting the CMD ["5"] with 10


---
apiVersion: v1 
kind: Pod 
metadata:
  name: ubuntu-sleeper-3 
spec:
  containers:
  - name: ubuntu
    image: ubuntu
    command:
      - /bin/bash
      - -c
      - sleep 1200;
````
#### The last pod definition will run /bin/bash -c sleep 1200 on startup


````

k run tools --image=harbor.fbclouddemo.us/publicimages/ubuntu-tools-focal@sha256:afa2c64fe8f029fe66b6dd88f0c307cb344e9fa24de4e64ca715038e94f02394 --dry-run=client -oyaml --command -- sleep 90000 > tools.yaml

````


````
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper-3
spec:
  containers:
  - name: ubuntu
    image: ubuntu
    command:
      - "sleep"
      - "1200"


---
apiVersion: v1
kind: Pod
metadata:
  name: webapp-green
  labels:
    name: webapp-green

spec:
  containers:
  - name: green-pod
    image: kodekloud/webapp-color
    args: ["--color", "green"]
````

#### Environment variables

````
apiVersion: v1
kind: Pod
metadata:
  name: webapp-green
  labels:
    name: webapp-green

spec:
  containers:
  - name: green-pod
    image: kodekloud/webapp-color
    ports:
      - containerPort: 8080
    env: //note this is equivalent to docker run -e APP_COLOR=pink myimage
      - name: APP_COLOR
        value: pink
````

#### ConfigMaps

````
kubectl create configmap app-config --from-literal=APP_COLOR=blue --from-literal=APP_MOD=prod

kubectl create configmap app-config --from-file=app_config.properties

---
apiVersion:v1
kind: ConfigMap
metadata:
  name: app-config

data:
  APP_COLOR: blue
  APP_MODE: prod

---

apiVersion: v1
kind: Pod
metadata:
  name: webapp-green
  labels:
    name: webapp-green

spec:
  containers:
  - name: green-pod
    image: kodekloud/webapp-color
    ports:
      - containerPort: 8080
    envFrom:
      - configMapRef:
          name: app-config
````

#### Secrets

````
kubectl create secret generic mysecret --from-literal-DB_Host=mysql --from-literal=DB_User=root --from-literal=DB_Password=paswrd

kubectl create secret generic mysecret --from-file=app_secret.properties

---
apiVersion: v1
kind: Secret
metadata:
  name: app-secret

data:
  DB_Host: bX1zcWw=
  DB_User: cm9vdA==
  DB_Password: cGFzd3KJ  //base64 encoded value for each

---
echo -n 'mysql' | base64

echo -n 'cGFzd3KJ' | base64 --decode

kubectl get secret mysecret -o yaml

---

apiVersion:v1
kind: ConfigMap
metadata:
  name: app-config

data:
  APP_COLOR: blue
  APP_MODE: prod

---

apiVersion: v1
kind: Pod
metadata:
  name: webapp-green
  labels:
    name: webapp-green

spec:
  containers:
  - name: green-pod
    image: kodekloud/webapp-color
    ports:
      - containerPort: 8080
    envFrom:
      - secretRef:
          name: app-secret

---
volumes:
- name: app-secret-volume 
  secret:
    secretName: app-secret
````

##### In the above the 3 files are created 1 with each secret as we have 3 defined in our file 

````
ls /opt/app-secret-volumes
cat /opt/app-secret-volumes/DB_Password
````

#### Multi-pod container

````
apiVersion: v1
kind: Pod
metadata:
  name: multi-container
  labels:
    name: multi-container

spec:
  containers:
  - name: green-pod
    image: kodekloud/webapp-color
    ports:
      - containerPort: 8080
  
  - name: log-agent
    image: log-agent

---

kubectl exec -i -t app -n elastic-stack -- /bin/sh
````

#### Design Patterns

1. Sidecar
2. Adapter
3. Ambassador

#### InitContainer

````
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox
    command: ['sh', '-c', 'git clone <some-repository-that-will-be-used-by-application> ;']

---

apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup myservice; do echo waiting for myservice; sleep 2; done;']
  - name: init-mydb
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup mydb; do echo waiting for mydb; sleep 2; done;']
````

#### Cluster Maintenance

#### OS Upgrades
#### 5 minute time out when nodes go down
#### kube-controller-manager --pod-eviction-timeout=5m0s(the default pod eviction timeout)

````
kubectl drain node-1 // this will drain and cordon the node

kubectl uncordon node-1

kubectl cordon node-2 // this will only mark the node as unscheduleable to new pods, existing pods stay

kubectl drain node01 --ignore-daemonsets

````

#### Kubernetes releases and versions
#### v1.11.3
#### v1 -> MAJOR -> 11 -> MINOR -> 3 -> PATCH
#### V1.0 was released in July 2015

#### Cluster upgrade process 

#### It is not mandatory that the kube components version match, but no component should be newer than the kube-api server component
#### Controller-Manager & kube-scheduler can be 1 version behind
#### kubelet & kube-proxy can be 2 version behind
#### so if kube-api is @ v1.10 Controller-Manager and kube-scheduler can be @ 1.9 or 1.10
#### kubelet and kube-proxy can be @ 1.8 or 1.9 or 1.10
#### kubectl can be at v1.11 or v1.10 or v1.9 or v1.8
#### k8s support the last 3 minor releases so if your cluster is on 1.10 and k8s releases 1.13 you are now out of support
#### as only v1.13, v1.12, and v1.11 is now supported
#### recommedation is to upgrade 1 minor version at a time, so v1.10 to v1.11

````
kubeadm upgrade plan

kubeadm upgrade apply

// control-plane node(s)
sudo apt upgrade -y kubeadm=1.12.0-00
sudo kubeadm upgrade apply v1.12.0
kubectl drain controlplane --ignore-daemonsets
sudo apt upgrade -y kubelet=1.12.0-00
sudo systemctl daemon-reload
sudo systemctl restart kubelet
kubectl uncordon controlplane

// Worker node(s)
kubectl drain node01
sudo apt upgrade -y kubeadm=1.12.0-00
kubectl drain controlplane --ignore-daemonsets
sudo kubeadm upgrade node
sudo apt upgrade -y kubelet=1.12.0-00
// sudo kubeadm upgrade node config --kubelet-version v1.12.0
sudo systemctl daemon-reload
sudo systemctl restart kubelet
kubectl uncordon node01

kubeadm token list

sudo apt update
sudo apt-cache madison kubeadm

````

````
// let's install a specific version

sudo apt-get install -y kubelet=1.21.0-00 kubeadm=1.21.0-00 kubectl=1.21.0-00

sudo apt-mark hold kubelet kubeadm kubectl

````

#### Remember kubeadm does not install or upgrade kubelet

#### Above example is a bit messy so let's try again
[kubeadm upgrade docs](https://v1-20.docs.kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/)

````
// let's start on our control plane node 

kubectl drain controlplane --ignore-daemonsets

apt update
apt-cache madison kubeadm

apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.20.0-00 && \
apt-mark hold kubeadm

kubeadm version

kubeadm upgrade plan

sudo kubeadm upgrade apply v1.20.8

sudo kubeadm upgrade apply v1.20.0 #just give me 1.20.0

// note I wanted to update to 1.20.0 but the upgrade plan says 1.20.8 so now I need to re-run the kubeadm upgrade to that version
// even though it says v1.20.8 you can apply the base v1.20.0

apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.20.8-00 && \
apt-mark hold kubeadm

kubeadm version

kubeadm upgrade plan

sudo kubeadm upgrade apply v1.20.8 //even though it says v1.20.8 you can apply the base v1.20.0

// after this if you have other controlplane nodes follow the instructions to drain and run the 'sudo kubeadm upgrade node'
// again follow the instuctions from the doc

apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.20.8-00 kubectl=1.20.8-00 && \
apt-mark hold kubelet kubectl

sudo systemctl daemon-reload
sudo systemctl restart kubelet

kubectl uncordon controlplane

// now move on to the worked node(s)

kubectl drain node01 --ignore-daemonsets

apt-mark unhold kubeadm && \
apt-get update && apt-get install -y kubeadm=1.20.8-00 && \
apt-mark hold kubeadm

sudo kubeadm upgrade node

apt-mark unhold kubelet kubectl && \
apt-get update && apt-get install -y kubelet=1.20.8-00 kubectl=1.20.8-00 && \
apt-mark hold kubelet kubectl

sudo systemctl daemon-reload
sudo systemctl restart kubelet

kubectl uncordon node01 // obviously needs to be run on the controlplane node(s)

kubectl get nodes

````

#### Backup ETCD

#### Backup Candidates

1. Resource Configuration(use declarative model and check into version control, or query API server)
2. etcd cluster - --data-dir=/var/lib/etcd
3.

````
kubectl get all -A -o yaml > all-deploy-services.yaml

export ETCDCTL_API=3

ETCDCTL_API=3 etcdctl snapshot save snapshot.db

ETCDCTL_API=3 etcdctl snapshot status snapshot.db

sudo systemctl stop kube-apiserver

ETCDCTL_API=3 etcdctl snapshot restore snapshot.db --data-dir /var/lib/etcd-from-backup 
// make sure to change etcd.service --data-dir to the new location /var/lib/etcd-from-backup

sudo systemctl daemon-reload

sudo systemctl restart etcd

sudo systemctl start kube-apiserver

// full etcd command

ETCDCTL_API=3 etcdctl snapshot save snapshot.db \
--endpoints=https://127.0.0.1:2379 \
--cacert=/etc/etcd/ca.crt \
--cert=/etc/etcd/etcd-server.crt \
--key=/etc/etcd/etcd-server.key


etcdctl snapshot restore /opt/snapshot-pre-boot.db --data-dir /var/lib/etcd-from-backup \
--endpoints=https://127.0.0.1:2379 \
--cacert=/etc/kubernetes/pki/etcd/ca.crt \
--cert=/etc/kubernetes/pki/etcd/server.crt \
--key=/etc/kubernetes/pki/etcd/server.key

````

#### If your etcd cluster and your kube-apiserver is a container, then there are no services to restart
#### Just do the below

````
// /etc/kubernetes/manifests/etcd.yaml

volumes:
  - hostPath:
      path: /var/lib/etcd-from-backup // this is what you set in the backup, make sure this is correct or it will not work
      type: DirectoryOrCreate
    name: etcd-data

---

// once you make the changes since this is a static pod it should restart automatically

docker ps -a // check to make sure all pods are up and running

kubectl get pods


Note 3: This is the simplest way to make sure that ETCD uses the restored data after the ETCD pod is recreated. You don't have to change anything else.

If you do change --data-dir to /var/lib/etcd-from-backup in the YAML file, make sure that the volumeMounts for etcd-data is updated as well, with the mountPath pointing to /var/lib/etcd-from-backup (THIS COMPLETE STEP IS OPTIONAL AND NEED NOT BE DONE FOR COMPLETING THE RESTORE)

````
#### Security Primitives

1. Authentication: who can access the apiserver
2. Authorization: what can they access, RBAC, etc.
3. K8s components use TLS 
4. Network Policy will allow you to control pod to pod communication

#### Authentication

#### k8s does not have the ability to create users, it can create serviceaccounts

1. Static Password file
2. Static Token file 
3. Certificates
4. Identity Services(like LDAP, kerberos, etc.)

#### We can create a users file with username and passwords

````
user-details.csv 

password123,user1,u0001
password123,user2,u0002
etc.

// optionally you can add 4th column for group

password123,user1,u0001,group1
password123,user2,u0002,group2
etc.

// we can also use a token file 

KpjCVBI7rCFAHYPkBYTIzRb7gu1cUc4B,user10,u0010,group1
KpjCVBI7rCFAHYPkBYTIzRb7gu1cUc4C,user11,u0011,group2
````

#### Then in kube-apiserver.service add option --basic-auth-file=user-details.csv
#### Then restart the service again if you are running the service on the host if you are running it as a container then edit the file in /etc/kubernetes/manifests and then the pod should restart with the new config

#### for token files add it to --token-auth-file=user-details.csv

````
curl -v -k https://master-node-ip:6443/api/v1/pods -u "user1:password123"

curl -v -k https://master-node-ip:6443/api/v1/pods -header "Authorization: Bearer KpjCVBI7rCFAHYPkBYTIzRb7gu1cUc4B"
````

````
kubectl create serviceaccount sa1
````

#### TLS 

#### Symmetric encryption uses the same key to encrypt as well as decrypt the message(note safe if you send it over the public internet)

#### Asymmetric encryption uses a pair of keys the private and public key(public lock)

#### This is all known as PKI

#### TLS in k8s

#### *.crt,*.pem(Public key) *.key,*-key.pem(Private key)

#### Server components that needs certs

#### kube-apiserver - apiserver.crt, apiserver.key
#### etcd-server- etcdserver.crt, etcdserver.key
#### kubelet server - kubelet.crt, kubelet.key

#### Client Certificates
#### admin - admin.crt, admin.key
#### kube-scheduler - scheduler.crt, scheduler.key
#### kube-controller-manager - controller-manager.crt, controller-manager.key
#### kube-proxy - kube-proxy.crt, kube-proxy.key


#### kube-api server is the only component that speaks to the etcd server so it is a client to etcd so it can use it's current key pair or you can generate a separate key-pair for it to speak to the etcd cluster - apiserver-etcd-client.crt, apiserver-etcd-client.key

#### kube-apiserver also speaks to the kubelet server and again it can use it's existing key-pair or we can generate new ones
#### apiserver-kubelet-client.crt, apiserver-kubelet-client.key

#### Let's generate our certs we can use openssl or a tool from cloudflare called cfssl(a lot of folks are moving to this one)

#### they also mentioned EASYRSA(never heard of it)

[cfssl](https://github.com/cloudflare/cfssl)

[easy-rsa](https://github.com/OpenVPN/easy-rsa)

[centos install script](https://github.com/fabianbrash/Bash/blob/master/install_cfssl.sh)


````

// generate our root ca private key

opnessl genrsa -out ca.key 2048

// generate our csr 

openssl req -new -key ca.key -subj "/CN=KUBERNETES-CA" -out ca.csr 

// these are for our clients

// now create our cert 

openssl x509 -req -in ca.csr -signkey ca.key -out ca.crt

// generate key-pair for our admin user 

openssl genrsa -out admin.key 2048

// generate our csr

openssl req -new -key admin.key -subj "/CN=kube-admin/O=system:masters" -out admin.csr 

// generate our cert 

openssl x509 -req -in admin.csr -CA ca.crt -CAkey ca.key -out admin.crt

// kube-scheduler you must prefix with system

// generate key-pair for our admin user 

openssl genrsa -out scheduler.key 2048

// generate our csr

openssl req -new -key scheduler.key -subj "/CN=system:kube-scheduler" -out admin.csr 

// generate our cert 

openssl x509 -req -in admin.csr -CA ca.crt -CAkey ca.key -out admin.crt


// same with kube-controller manager

curl https://kube-apiserver:6443/api/v1/pods ---key admin.key --cert admin.crt --cacert ca.crt

// our create a kube-config.yaml file

apiVersion: v1 
clusters:
- cluster:
    certificate-authority: ca.crt
    server: https://kube-apiserver:6443
  name: kubernetes
kind: Config
users:
- name: kubernetes-admin
  user:
    client-certificate: admin.crt
    client-key: admin.key


// now our server Certificates

// etcd 


// generate key-pair for our admin user 

openssl genrsa -out etcdserver.key 2048

// generate our csr

openssl req -new -key etcdserver.key -subj "/CN=etcd-server" -out etcdserver.csr 

// generate our cert 

openssl x509 -req -in etcdserver.csr -CA ca.crt -CAkey ca.key -out etcdserver.crt

// now because etcd can be clustered if it is you will have to generate a peer certificate as well for each node

// ex. etcdpeer1.key, etcdpeer1.crt, etcdpeer2.key, etcdpeer2.crt again from our CA or you can create a separate CA for etcd cluster certs 

// stored in etcd.yaml since we are using static pods this is located in /etc/kubernetes/manifests

// now the Kube-apiserver apiserver.crt apiserver.key 
// SAN must have all the names: kubernetes, kubernetes.default, kubernetes.default.svc, kubernetes.default.svc.cluster.local, 10.96.0./// 1, 172.17.0.87(again replace with your IPs)

// to use SAN with openssl you must create a config file(don't I know this well)

openssl genrsa -out apiserver.key 2048
opnessl req -new -key apiserver.key -subj "/CN=kube-apiserver" -out apiserver.csr --config openssl.cnf

// file is called openssl.cnf 

[req]
req_extensions = v3_req
[v3_req]
basicConstraints = CA:FALSE 
keyUsage = nonRepudiation,
subjectAltName = @alt_names
[alt_names]
DNS.1 = kubernetes
DNS.2 = kubernetes.default
DNS.3 = kubernetes.default.svc
DNS.4 = kubernetes.default.svc.cluster.local
IP.1 = 10.96.0.1
IP.2 = 172.17.0.87

openssl x509 -req -in apiserver.csr -CA ca.crt -CAkey ca.key -out apiserver.crt

// now these are stored in the kube-apiserver .service file or when you run the executable(which would be insane)
// also if the kube-apiserver is a static pod then these options will be declaated in the .yaml file located @
// /etc/kubernetes/manifests

// kubelet 
// generate kubelet.crt and kubelet.key each nodes kubelet must have it's own name ex. node1, node2, node2 
// so we need to generate separate set of certs kubelet-node01.crt and .key, kubelet-node02.crt and .key etc.

// kubelet-config.yaml(node01)

kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
authentication:
  x509:
    clientCAFile: "/var/lib/kubernetes/ca.pem"
authorization:
  mode: Webhook
clusterDomain: "cluster.local"
clusterDNS:
  - "10.32.0.10" // again change for your env 
podCIDR: "${POD_CIDR}"
resolvConf: "/run/systemd/resolve/resolvve.conf"
runtimeRequestTimeout: "15m"
tlsCertFile: "/var/lib/kubelet/kubelet-node01.crt"
tlsPrivateKeyFile: "/var/lib/kubelet/kubelet-node01.key"

// we also need to generate kubelet-client.crt and kubelet-client.key
// CN must be system:mode:node01 and for node02 system:node:node02 etc..
// also O must be set to system:nodes

````

````
cat /etc/systemd/system/kube-apiserver.service // the hardway

cat /etc/kubernetes/manifests/kube-apiserver.yaml  // kubeadm

openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text -noout

journalctl -u etcd.service -l  // hardway cluster

kubectl logs etcd-master // kubeadm

docker logs container_id  // if kube-api is down

````

#### Certificates API

````
openssl genrsa -out jane.key 2048

openssl req -new -key jane.key -subj "/CN=jane" -out jane.csr  // note we can also add a group here if we want

openssl req -new -key jane.key -subj "/CN=jane/O=Devs"


---
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: jane

spec:
  groups:
  - system:authenticated
  usages:
  - digital signature
  - key encipherment
  - client auth
  request: base64 encoded csr // cat jane.csr | base64
  signerName: kubernetes.io/kube-apiserver-client

---

cat jane.csr | base64 | tr -d '\n' # remove newlines

kubectl get csr

kubectl certificate approve jane

kubectl get csr jane -o yaml // look under status  > certificate this is base64 encoded so will have to be decoded

// echo "LS0...." | base64 --decode
// cat myuser.csr | base64 | tr -d "\n"

kubectl get csr myuser -o jsonpath='{.status.certificate}'| base64 -d > myuser.crt

kubectl create clusterrolebinding jane-admin --clusterrole=cluster-admin --user=jane # make jane a cluster admin

````

#### Contexts ties a user to a cluster, a kubeconfig file has 3 parts

1. Clusters
2. Users
3. Contexts

### Example jane@google context would tie user jane with the cluster she has in Google

### To make the above clearer say we have a cluster named google-cluster and account jane5 we could make a context called

### user@cluster so our context would be jane5@google-cluster


````
---
apiVersion: v1
kind: Config

current-context: jane5@google-cluster

clusters:
- name: google-cluster
  cluster:
    #certificate-authority: /etc/pki/certs/ca.crt
    certificate-authority-data: base64 encoded version of ca.crt
    server: https://google-cluster:6443

contexts:
- name: jane5@google-cluster
  context:
    cluster: google-cluster
    user: jane5
    #namespace: finance optional when I swithc to this context go to the finance namespace

users:
- name: jane5
  user:
    client-certificate: /home/jane5/.kube/jane5.crt
    client-key: /home/jane5/.kube/jane5.key


````

### Now let's create config file for jane 

````
#Set cluster info

kubectl config --kubeconfig=tap-onprem set-cluster tap-onprem --server=https://KUBE_API_IP:6443 --certificate-authority=/home/jane/k8s_users/ca.crt

#Set user info

kubectl config --kubeconfig=tap-onprem set-credentials jane --client-certificate=/home/jane/k8s_users/jane.crt --client-key=/home/jane/k8s_users/jane.key

#Create new context "tap-onprem"

kubectl config --kubeconfig=tap-onprem set-context tap-onprem --cluster=tap-onprem --user=jane

#Set current context

kubectl config --kubeconfig=tap-onprem use-context tap-onprem

cp tap-onprem ~/.kube/config 

cp jane.crt jane.key ca.crt ~/.kube

````

#### Note in the above you have to move the certs into the .kube directory for this to work there is an option to embed the certs that I need to figure out how to do, I think you can just cat the certs and base64 encode them into the locations in the file, but I need to test that out.

##### Also to get the ca.crt file I simply pulled it from the admins kubeconfig file and base64 decoded it into the ca.crt file


[https://www.nutellino.it/2019/12/18/new-administrator-user-on-kubernetes/](https://www.nutellino.it/2019/12/18/new-administrator-user-on-kubernetes/)



#### All certificate operations are carried out by the Controller Manager(some components are CSR-APPROVING, and CSR-SIGNING)

#### In /etc/kubernetes/manifests/kube-controller-manager.yaml there are 2 options --cluster-signing-cert-file and --cluster-signing-key-file


##### etcd.yaml config snapshot

````
- command:
    - etcd
    - --advertise-client-urls=https://172.17.0.8:2379
    - --cert-file=/etc/kubernetes/pki/etcd/server.crt
    - --client-cert-auth=true
    - --data-dir=/var/lib/etcd
    - --initial-advertise-peer-urls=https://172.17.0.8:2380
    - --initial-cluster=controlplane=https://172.17.0.8:2380
    - --key-file=/etc/kubernetes/pki/etcd/server.key
    - --listen-client-urls=https://127.0.0.1:2379,https://172.17.0.8:2379
    - --listen-metrics-urls=http://127.0.0.1:2381
    - --listen-peer-urls=https://172.17.0.8:2380
    - --name=controlplane
    - --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt
    - --peer-client-cert-auth=true
    - --peer-key-file=/etc/kubernetes/pki/etcd/peer.key
    - --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt
    - --snapshot-count=10000
    - --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt

````

##### kube-apiserver.yaml config snapshot

````
- command:
    - kube-apiserver
    - --advertise-address=172.17.0.8
    - --allow-privileged=true
    - --authorization-mode=Node,RBAC
    - --client-ca-file=/etc/kubernetes/pki/ca.crt
    - --enable-admission-plugins=NodeRestriction
    - --enable-bootstrap-token-auth=true
    - --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt
    - --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt
    - --etcd-keyfile=/etc/kubernetes/pki/apiserver-etcd-client.key
    - --etcd-servers=https://127.0.0.1:2379
    - --insecure-port=0
    - --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-kubelet-client.crt
    - --kubelet-client-key=/etc/kubernetes/pki/apiserver-kubelet-client.key
    - --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname
    - --proxy-client-cert-file=/etc/kubernetes/pki/front-proxy-client.crt
    - --proxy-client-key-file=/etc/kubernetes/pki/front-proxy-client.key
    - --requestheader-allowed-names=front-proxy-client
    - --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt
    - --requestheader-extra-headers-prefix=X-Remote-Extra-
    - --requestheader-group-headers=X-Remote-Group
    - --requestheader-username-headers=X-Remote-User
    - --secure-port=6443
    - --service-account-key-file=/etc/kubernetes/pki/sa.pub
    - --service-cluster-ip-range=10.96.0.0/12
    - --tls-cert-file=/etc/kubernetes/pki/apiserver.crt
    - --tls-private-key-file=/etc/kubernetes/pki/apiserver.key
````

##### kube-controller-manager.yaml config snapshot

````
- command:
    - kube-controller-manager
    - --allocate-node-cidrs=true
    - --authentication-kubeconfig=/etc/kubernetes/controller-manager.conf
    - --authorization-kubeconfig=/etc/kubernetes/controller-manager.conf
    - --bind-address=127.0.0.1
    - --client-ca-file=/etc/kubernetes/pki/ca.crt
    - --cluster-cidr=10.244.0.0/16
    - --cluster-name=kubernetes
    - --cluster-signing-cert-file=/etc/kubernetes/pki/ca.crt
    - --cluster-signing-key-file=/etc/kubernetes/pki/ca.key
    - --controllers=*,bootstrapsigner,tokencleaner
    - --kubeconfig=/etc/kubernetes/controller-manager.conf
    - --leader-elect=true
    - --node-cidr-mask-size=24
    - --port=0
    - --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt
    - --root-ca-file=/etc/kubernetes/pki/ca.crt
    - --service-account-private-key-file=/etc/kubernetes/pki/sa.key
    - --service-cluster-ip-range=10.96.0.0/12
    - --use-service-account-credentials=true
````

##### kube-scheduler.yaml config snapshot

````
- command:
    - kube-scheduler
    - --authentication-kubeconfig=/etc/kubernetes/scheduler.conf
    - --authorization-kubeconfig=/etc/kubernetes/scheduler.conf
    - --bind-address=127.0.0.1
    - --kubeconfig=/etc/kubernetes/scheduler.conf
    - --leader-elect=true
    - --port=0
````

#### KubeConfig

````
kubectl get pods --server mycluster:6443 --client-key admin.key --client-certificate admin.crt --certficate-authority ca.crt

kubectl get pods --kubeconfig config // move above option to the file config

````

#### Kubeconfig file has 3 sections
1. Clusters
2. Users
3. Contexts - marries users to clusters

````
apiVersion: v1
kind: Config

current-context: my-DO-admin@my-DO-cluster

clusters:
- name: my-cluster
  cluster:
    certificate-authority: /etc/kubernetes/pki/ca.crt
    server: https://mycluster:6443
- name: DO-cluster
  cluster:
    certificate-authority: /etc/kubernetes/pki/ca.crt
    server: https://my-DO:6443 


contexts:
- name: my-cluster-admin@my-cluster
  context:
    cluster: my-cluster
    user: my-cluster-admin
    namespace: dev

- name: my-DO-admin@my-DO-cluster
  context:
    cluster: DO-cluster
    user: DO-cluster-admin

users:

- name: my-cluster-admin
  user:
    client-certificate: admin.crt
    client-key: admin.key

- name: DO-cluster-admin
  user:
    client-certificate: /path/to/my/cert/admin.crt
    client-key: admin.key



---


apiVersion: v1
kind: Config

current-context: my-DO-admin@my-DO-cluster

clusters:
- name: my-cluster
  cluster:
    certificate-authority-data: LS0...... // cat /etc/kubernetes/pki/ca.crt | base64
    server: https://mycluster:6443
- name: DO-cluster
  cluster:
    certificate-authority: /etc/kubernetes/pki/ca.crt
    server: https://my-DO:6443 


contexts:
- name: my-cluster-admin@my-cluster
  context:
    cluster: my-cluster
    user: my-cluster-admin
    namespace: dev

- name: my-DO-admin@my-DO-cluster
  context:
    cluster: DO-cluster
    user: DO-cluster-admin

users:

- name: my-cluster-admin
  user:
    client-certificate: admin.crt
    client-key: admin.key

- name: DO-cluster-admin
  user:
    client-certificate: /path/to/my/cert/admin.crt
    client-key: admin.key
````

````
kubectl config view

kubectl config view --kubeconfig=my-custom-config

kubectl config use-context my-cluster-admin@my-cluster

kubectl config current-context

kubectl config --kubeconfig=/root/my-kube-config use-context research

kubectl config --kubeconfig=/root/my-kube-config current-context
````

#### API Groups

````
curl https://kube-master:6443/version

curl https://kube-master:6443/api/v1/pods
````

#### Api grouping

1. /metrics
2. /healthz
3. /version
4. /api - core group
5. /apis - named group(this is the future state)
6. /logs

#### /api - the core group

##### /api/v1{namespaces,pods,rc,events,endpoints,nodes,bindings,PV,PVC,configmaps,secrets,services}

#### /apis note this is where all new apis will be built going forward

##### /apis{/apps,/extensions,/networking.k8s.io,/storage.k8s.io,/authentication.k8s.io,/certificates.k8s.io}

##### /apps{/v1/{deployments,/replicasets,/statefulsets}} -verbs list,get,create,delete,update,watch

##### /networking.k8s.io/v1/networkpolicies

##### /certificates.k8s.io/v1/certificatesigningrequests

````
curl http://localhost:6443 -k

curl http://localhost:6443/apis -k | grep "name"

curl http://localhost:6443 -k --key admin.key --cert admin.crt --cacert ca.crt

kubectl proxy #this will setup a proxy to the kube-api server and then use your creds in ~/.kube/config

// then you can just run the below command and have access to kube-api server with having to specify your creds

curl http://localhost:8001 -k
````

#### Authorization

#### 4 types

1. Node Authorizer - nodes use this
2. ABAC - use JSON format any change requires a reboot of the API server, so this is not used much, RBAC is better
3. RBAC
4. Webhook - external authorization, ex. Open Policy Agent 
5. AlwaysAllow
6. AllwaysDeny

#### Mode is set on the kube-apiserver config option --authorization-mode=AlwaysAllow(this is also the default)

#### also you can do this --authorization-mode=Node,RBAC,Webhook so your cluster can use any of the 3

#### Note in the above when a user tries to get authorization it will go in order, Node, RBAC, Webhook

````
{
  "kind": "Policy",
  "spec": {
    "user": "dev-user",
    "namespace": "*",
    "resource": "pods",
    "apiGroup": "*"
  }
}


{
  "kind": "Policy",
  "spec": {
    "group": "dev-users",
    "namespace": "*",
    "resource": "pods",
    "apiGroup": "*"
  }
}

````


#### RBAC


````
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer

rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]

- apiGroups: [""]
  resources: ["ConfigMaps"]
  verbs: ["create"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer
  namespace: dev

rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]

- apiGroups: [""]
  resources: ["ConfigMaps"]
  verbs: ["create"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer

rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]

- apiGroups: [""]
  resources: ["ConfigMaps"]
  verbs: ["create"]
  resourceNames: ["blue", "orange"]  //only grant access to the 'blue' and 'orange' pod(s)

// now lets bind our above role

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: devuser-developer-binding
subjects:
- kind: User
  name: dev-user
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io


---

---

apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer


rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "delete"]



---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: dev-user-binding

subjects:
- kind: User
  name: dev-user
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io 

kubectl auth can-i create deployments

kubectl auth can-i create deployments --as dev-user
````

````
# Please edit the object below. Lines beginning with a '#' will be ignored,
# and an empty file will abort the edit. If an error occurs while saving this file will be
# reopened with the relevant failures.
#
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  creationTimestamp: "2021-07-16T14:16:16Z"
  name: developer
  namespace: blue
  resourceVersion: "2775"
  uid: 9e60c6b6-8949-4a52-b399-7a7c41f23e2b
rules:
- apiGroups:
  - ""
  resourceNames:
  - dark-blue-app
  resources:
  - pods
  verbs:
  - get
  - watch
  - create
  - delete
  
- apiGroups:
  - apps
  - extensions
  resources:
  - deployments
  verbs:
  - create
````

#### Cluster Roles

````
kubectl api-resources --namespaced=true

kubectl api-resources --namespaced=false


---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-admin

rules:
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["list", "get", "create", "delete"]


---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cluster-admin-role-binding
subjects:
- kind: User
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-admin
  apiGroup: rbac.authorization.k8s.io

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: node-admin

rules:
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["list"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: node-admin

rules:
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["list"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: node-admin-role-binding
subjects:
- kind: User
  name: michelle
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: node-admin
  apiGroup: rbac.authorization.k8s.io

---

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: storage-admin

rules:
- apiGroups: [""]
  resources: ["persistentvolumes", "storageclasses"]
  verbs: ["list"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: michelle-storage-admin
subjects:
- kind: User
  name: michelle
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: storage-admin
  apiGroup: rbac.authorization.k8s.io


---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: storage-admin

rules:
- apiGroups: [""]
  resources: ["persistentvolumeclaims"]
  verbs: ["list", "get", "create", "update", "patch", "delete"]

- apiGroups: [""]
  resources: ["persistentvolumes"]
  verbs: ["list", "get", "create", "update", "patch", "delete"]

- apiGroups: ["storage.k8s.io"]
  resources: ["storageclasses"]
  verbs: ["list", "get", "create", "update", "patch", "delete"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: michelle-storage-admin

subjects:
- kind: User
  name: michelle
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: storage-admin
  apiGroup: rbac.authorization.k8s.io

````


#### Service accounts

````
apiVersion: v1
kind: Pod
metadata:
  name: sa-pod 

spec:
  containers:
    - name: sa-pod
      image: nginx
  serviceAccountName: my-service-account
  #automountServiceAccountToken: false optional if you do not want to mount the sa token

````

````

---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: read-pods
  namespace: default
subjects:
- kind: ServiceAccount
  name: dashboard-sa # Name is case sensitive
  namespace: default
roleRef:
  kind: Role #this must be Role or ClusterRole
  name: pod-reader # this must match the name of the Role or ClusterRole you wish to bind to
  apiGroup: rbac.authorization.k8s.io

````

#### Image security

````
apiVersion: v1
kind: Pod
metadata:
  name: private-pod

spec:
  containers:
  - name: nginx
    image: priv-reg.io/apps/nginx
    imagePullSecrets: 
    - name: regcred


---
kubectl create secret docker-registry regcred \
--docker-server=priv-reg.io \
--docker-username=registry-user \
--docker-password=registry-password \
--docker-email=registry-user@org.com
````

````
image: nginx #this is actually
image: docker.io/library/nginx  #docker.io is the official docker registry and library is the user/account where all official images are stored, you would replace library with your username i.e. docker.io/fabianbrash/nginx

````

#### Note we can also export the config.json file that is stored when you log into your registry and use that to create the secret we just have to base64 encode the data the file is usually in your home directory ~/.docker/config.json

````
apiVersion: v1
kind: Secret
metadata:
  name: do-cr
  namespace: default
  labels:
    tier: prod
data:
  .dockerconfigjson: ewoxwerfdfhgkdjfjgkgkdjkdjdf==
type: kubernetes.io/dockerconfigjson  
````

````
cat ~/.docker/config.json | base64

````

#### Docker security

#### see capabilities /usr/include/linux/capability.h

````
docker run --privileged ubuntu //run container with all privileges

docker run --cap-add MAC_ADMIN ubuntu

docker run --cap-drop KILL ubuntu
````

#### Security context

````
apiVersion: v1
kind: Pod
metadata:
  name: web-pod

spec:
  securityContext: // this is at the pod level so all containers in this pod will run as user 1000
    runAsUser: 1000
  containers:
  - name: ubuntu
    image: ubuntu
    command: ["sleep", "3600"]
    securityContext:
      runAsUser: 1000
      capabilities:
        add: ["MAC_ADMIN"] // capabilities are only supported at the container level not at the POD level
````

#### Network Policies


````
policyTypes:
- Ingress
ingress:
- from:
  - podSelector:
      matchLabels:
        name: api-pod
  ports:
  - protocol: TCP
    port: 3306

---
apiVersion: network.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy

spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          name: api-pod
    ports:
    - protocol: TCP
      port: 3306
````

#### Network policies not supported by Flannel but is supported by Kube-router,Calico,Romana,Weave-net

````
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy

spec:
  podSelector:
    matchLabels:
      role: db

  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector: // if you remove the podSelector and just leave the namespaceSelector all pods in that namespace can connect db
        matchLabels:
          name: api-pod
      namespaceSelector:
        matchLabels:
          name: prod // this label must exist on the namespace
    - ipBlock:
        cidr: 192.168.5.10/32
    ports:
    - protocol: TCP
      port: 3306

---


apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy

spec:
  podSelector:
    matchLabels:
      role: db

  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector: // if you remove the podSelector and just leave the namespaceSelector all pods in that namespace can connect db
        matchLabels:
          name: api-pod
    - namespaceSelector: // now all 3 are OR logical operators access will be granted to pod || namespace || ipBlock
        matchLabels:
          name: prod // this label must exist on the namespace
    - ipBlock:
        cidr: 192.168.5.10/32
    ports:
    - protocol: TCP
      port: 3306

---

apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy

spec:
  podSelector:
    matchLabels:
      role: db

  policyTypes:
  - Ingress
  - Egress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          name: api-pod
    ports:
    - protocol: TCP
      port: 3306
  egress:
  - to:
    - ipBlock:
        cidr: 192.168.5.10/32
    ports:
    - protocol: TCP
      port: 80
````

````
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: internal-policy

spec:
  podSelector:
    matchLabels:
      name: internal
  policyTypes:
  - Egress
  egress:
  - to:
    - podSelector:
        matchLabels:
          name: payroll
    
    ports:
    - protocol: TCP
      port: 8080
  - to:
    - podSelector:
        matchLabels:
          name: mysql
    ports:
    - protocol: TCP
      port: 3306
````


#### Storage

#### /var/lib/docker docker root directory
#### image layer is read-only container layer is writeable  but only for the life of the container
## Docker uses COPY-ON-WRITE(COW) when you make a change to a file in the image layer it's copied to the container layer so it's writeable

````
docker run \
--mount type=bind,source=/data/mysql,target=/var/lib/mysql \
mysql

// the above is preferred to

docker run -v /data/mysql:/var/lib/mysql mysql

````

#### Storage Drivers

1. AUFS
2. ZFS
3. BTRFS
4. Device Mapper
5. Overlay
6. Overlay 2

#### Volume Drivers handled by volume driver plugin

1. Local(default)
2. Azure File Storage
3. Convoy
4. Digital Ocean Block Storage
5. Flocker
6. gce-docker
7. GlusterFS
8. NetApp
9. RexRay(this allows you to provision on multiple providers)
10. Portworx
11. VMware vSphere Storage

````
docker run -it \
--name mysql \
--volume-driver rexray/ebs \
--mount src=ebs-vol,target=/var/lib/mysql \
mysql

// the above uses the rexray volume driver to provision an ebs volume
````

#### Container Storage Interface(CSI)

#### Side note Container Runtime Interface(CRI) Container Network Interface(CNI)

#### Volumes in k8s

````
apiVersion: v1
kind: Pod
metadata:
  name: random-number-generator
spec:
  containers:
  - image: alpine
    name: alpine
    command: ["/bin/sh", "-c"]
    args: ["shuf -i 0-100 -n 1 >> /opt/number.out;"]
    volumeMounts:
    - mountPath: /opt
      name: data-volume
  
  volumes:
  - name: data-volume
    hostPath:
      path: /data
      type: Directory

// AWS version

apiVersion: v1
kind: Pod
metadata:
  name: random-number-generator
spec:
  containers:
  - image: alpine
    name: alpine
    command: ["/bin/sh", "-c"]
    args: ["shuf -i 0-100 -n 1 >> /opt/number.out;"]
    volumeMounts:
    - mountPath: /opt
      name: data-volume
  
  volumes:
  - name: data-volume
    awsElasticBlockStore:
      volumeID: <volume-id>
      fsType: ext4
````

#### Persistent volumes

````
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-vol1

spec:
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 1Gi
  hostPath:
    path: /tmp/data

// aws version

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-vol1

spec:
  accessModes:
    - ReadWriteOnce
  capacity:
    storage: 1Gi
  awsElasticBlockStore:
    volumeID: <volume-id>
    fsType: ext4

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-log

spec:
  accessModes:
    - ReadWriteMany
  capacity:
    storage: 100Mi
  hostPath:
    path: /pv/log
  persistentVolumeReclaimPolicy: Retain
````

#### Persistent Volume Claims

````
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-claim

spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 500Mi

---

apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
````

#### Storage Classes

````
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: google-storage

provisioner: kubernetes.io/gce-pd

parameters:  // optional params
  type: pd-standard
  replication-type: none

---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: google-storage

provisioner: kubernetes.io/gce-pd

volumeBindingMode: WaitForFirstConsumer


---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  storageClassName: google-storage
  resources:
    requests:
      storage: 500Mi
````


````

The Storage Class called local-storage makes use of VolumeBindingMode set to WaitForFirstConsumer. This will delay the binding and provisioning of a PersistentVolume until a Pod using the PersistentVolumeClaim is created.



````

#### Networking

````
ip link

ip addr add 192.168.1.11/24 dev eth0

route

ip route add 192.168.2.0/24 via 192.168.1.1

ip route add 172.217.194.0/24 via 192.168.2.1

ip route add default via 192.168.2.1

cat /proc/sys/net/ipv4/ip_forward

// default value is 0

echo 1 > /proc/sys/net/ipv4/ip_forward

/etc/sysctl.conf // make change net.ipv4.ip_forward = 1
````

#### DNS

````
/etc/resolve.conf

/etc/nsswitch.conf

// search mycompany.com - this adds a search domain to our resolve.conf file

````

#### Note that nslookup and dig does not look at your local /etc/hosts file when it does a lookup, so if you add an entry there and do a nslookup it will not show up

#### Network Namespaces

````
ip netns add red

ip netns add red

ip netns

ip netns exec red ip link

ip -n red link

arp

ip netns exec red arp

route

ip netns exec red route

ip link add veth-red type veth peer name veth-blue
ip link set veth-red netns red
ip link set veth-blue netns blue

ip -n red addr add 192.168.15.1 dev veth-red
ip -n blue addr add 192.168.15.2 dev veth-blue

ip -n red link set veth-red up
ip -n blue set veth-blue up

ip netns exec red ping 192.168.15.2

ip netns exec red arp

// Linux Bridge

ip link add v-net-0 type bridge
ip link set dev v-net-0 up

ip -n red link del veth-red // this deletes both pairs

ip link add veth-red type veth peer name veth-red-br
ip link add veth-blue type veth peer name veth-blue-br
ip link set veth-red netns red
ip link set veth-red-br master v-net-0
ip link set veth-blue netns blue
ip link set veth-blue-br master v-net-0

ip -n red addr add 192.168.15.1 dev veth-red
ip -n blue addr add 192.168.15.2 dev veth-blue

ip -n red link set veth-red up
ip -n blue set veth-blue up

ip addr add 192.168.15.5/24 dev v-net-0

ip netns exec blue ip route add 192.168.1.0/24 via 192.168.15.5

iptables -t nat -A POSTROUTING -s 192.168.15.0/24 -j MASQUERADE

ip netns exec blue ip route add default via 192.168.15.5

iptables -t nat -A PREROUTING --dport 80 --to-destination 192.168.15.2 -j DNAT

````

#### Networking in Docker

````
docker run --network none nginx

docker run --network host nginx
````

#### CNI

#### K8s launches each pod with the network set to none if you are using docker as your runtime, then it attaches it to the namespace so it can use CNI


#### K8s required ports

1. kube-api - 6443
2. kubelet - 10250
3. kube-scheduler - 10251
4. kube-controller-manager - 10252
5. services - 30000-32767
6. etcd - 2379
7. etcd clients - 2380(in a clustered setup of etcd)


#### Pod Networking

1. PODs must have it's own IP
2. PODs must be able to talk to each other
3. PODs must be able to talk to each other on the same node or different nodes without NAT

#### --cni-conf-dir=/etc/cni/net.d, --cni-bin-dir=/etc/cni/bin

#### CNI in k8s

#### CNI is configured in the kubelet kubelet.service --network-plugin=cni, --cni-bin-dir=/opt/cni/bin, --cni-conf-dir=/etc/cni/net.d

````
ps -aux | grep kubelet
````

#### Weave-net deployment issues

#### You might see only pod 1 of 2 come up, check the logs and if you see an error about the networks are overlapping then do the below

````
  kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.IPALLOC_RANGE=10.32.0.0/16"

````

[GitHub issue](https://github.com/weaveworks/weave/issues/2736)

[Weaves docs](https://www.weave.works/docs/net/latest/kubernetes/kube-addon/#-changing-configuration-options)

#### The important bit is the IPALLOC_RANGE fix if it overlaps with you're current setup

#### CNI has 2 modules for IPAM DHCP or host-local

#### Weaveworks defaults to 10.32.0.0/12 range of 10.32.0.1 - 10.47.255.254 or 1,048,574 addresses again this overlapped in our environment so we had to change it to 10.32.0.0/16

#### So for one of the lab questions I needed to run the ip route command inside of a container so I ran the below command

````
kubectl run ub -i -t --image=ubuntu
````

#### Then I attempted to run the ip route command, which ip was not installed, so I found the package iproute2 and attempted an install, but if said it could not find the package, solution below

````
apt update && apt upgrade -y

apt install -y iproute2
````

#### The sysyem had to be updated first before I could install the iproute2 package

#### Service Networking

#### services go thru kube-proxy proxy modes are

1. userspace
2. iptables(default)
3. ipvs

````
kube-proxy --proxy-mode [userspace | iptables | ipvs] ...

ps aux | grep kube-api-server

iptables -L -t nat | grep db-service

cat /var/log/kube-proxy.log

// check to see what proxy mode is enabled

kubectl logs kube-proxy-2qkp9(substitute your pod name here) -n kube-system

// check to see what pod network pods have

kubectl logs weave-net-v64pd weave -n kube-system

// the above the cluster is using weave, substitute as needed
````

#### the ip range for services are defined in the kube-api-server --service-cluster-ip-range ipNet(default: 10.0.0.0/24)

#### DNS in K8s

#### web-service.apps - accessing a service in the app namespace or by web-service.apps.svc or by web-service.apps.svc.cluster.local

#### by defauly pods don't get dns entries but that can be enabled name would be 10-244-2-5 for a pod whose IP is 10.244.2.5

#### so the pod in app ns would be 10-244-2-5.apps.pod.cluster.local

#### CoreDNS in K8s

#### Prior to version 1.12 the DNS solution was kube-dns after 1.12 it's CoreDNS

#### /etc/coredns/Corefile CoreDNS uses the kubernetes plugin, injected into the pods via a configmap

#### 'pods insecure' is the option to enable pod dns entries

#### Managed by the kubelet

````
kubectl exec -it hr -- nslookup mysql.payroll > /root/CKA/nslookup.out
````


#### Ingress

````
apiVersion: extensions/v1beta1(I think this is out of beta now )
kind: Ingress
metadata:
  name: ingress-wear
spec:
  backend:
    serviceName: wear-service
    servicePort: 80

---

apiVersion: extensions/v1beta1(I think this is out of beta now )
kind: Ingress
metadata:
  name: ingress-wear-watch
spec:
  rules:
  - http:
      paths:
      - path: /wear
        backend:
          serviceName: wear-service
          servicePort: 80

      - path: /watch
        backend:
          serviceName: watch-service
          servicePort: 80    

---
apiVersion: extensions/v1beta1(I think this is out of beta now )
kind: Ingress
metadata:
  name: ingress-wear-watch
spec:
  rules:
  - host: wear.my-online-store.com
    http:
      paths:
      - backend:
          serviceName: wear-service
          servicePort: 80
  - host: watch.my-onlone-store.com
    http:
      paths:
      - backend:
          serviceName: watch-service
          servicePort: 80    
````

#### Changes to ingress as of k8s 1.20+

#### we can create an Ingress resource from the imperative way like this

````
kubectl create ingress <ingress-name> --rule="host/path=service:port"

kubectl create ingress ingress-test --rule="wear.my-online-store.com/wear*=wear-service:80"

````

[https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-em-ingress-em- ](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-em-ingress-em-)

[https://kubernetes.io/docs/concepts/services-networking/ingress](https://kubernetes.io/docs/concepts/services-networking/ingress)

[https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types](https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types)

#### URL rewrite

````
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  namespace: critical-space
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /pay
        backend:
          serviceName: pay-service
          servicePort: 8282
````

````
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: minimal-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test-ingress
  namespace: critical-space
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - http:
      paths:
      - path: /pay
        pathType: Prefix
        backend:
          service: 
            name: pay-service
            port:
              number: 8282
````

````
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  name: rewrite
  namespace: default
spec:
  rules:
  - host: rewrite.bar.com
    http:
      paths:
      - backend:
          serviceName: http-svc
          servicePort: 80
        path: /something(/|$)(.*)
````


````

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
  name: ingress-wear-watch
  namespace: app-space
spec:
  rules:
  - http:
      paths:
      - backend:
          service:
            name: wear-service
            port: 
              number: 8080
        path: /wear
        pathType: Prefix
      - backend:
          service:
            name: video-service
            port: 
              number: 8080
        path: /stream
        pathType: Prefix
      - backend:
          service:
            name: food-service
            port: 
              number: 8080
        path: /eat
        pathType: Prefix

````

[https://kubernetes.github.io/ingress-nginx/examples/rewrite/](https://kubernetes.github.io/ingress-nginx/examples/rewrite/)

#### nginx ingress working deployment

````
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ingress-controller
  namespace: ingress-space
spec:
  replicas: 1
  selector:
    matchLabels:
      name: nginx-ingress
  template:
    metadata:
      labels:
        name: nginx-ingress
    spec:
      serviceAccountName: ingress-serviceaccount
      containers:
        - name: nginx-ingress-controller
          image: quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.21.0
          args:
            - /nginx-ingress-controller
            - --configmap=$(POD_NAMESPACE)/nginx-configuration
            - --default-backend-service=app-space/default-http-backend
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          ports:
            - name: http
              containerPort: 80
            - name: https
              containerPort: 443
````

#### Designing & Choosing Kubernetes Cluster/Infrastructure

#### Configure HA

#### Master node hosts
1. etcd - (active, active)
2. api server - (active, active)
3. controller manager - (active, standby)
4. scheduler- (active, standby)

#### kube-controller-manager --leader-elect true(default is true)

#### during leader elect they try to get a lock on Kube-controller-manager Endpoint, whoever wins is the leader

#### --leader-elect-lease-duration 15s(default)

#### --leader-elect-renew-deadline 10s(default)

#### --leader-elect-retry-period 2s(default)

#### api server is the only component that speaks to etcd

#### --etcd-servers=https://IP_OR_DNS:2379,https://IP_OR_DNS:2379

#### ETCD HA

1. Only the leader can write
2. etcd uses RAFT for leader election
3. Quorum - N/2+1
4. Best practice is to use odd number of nodes

````
Instances      Quorum      Fault Tolerance
1              1           0
2              2           0
3              2           1
4              3           1
5              3           2
6              4           2
7              4           3

---

/// etcd.service

--initial-cluster peer-1=https://${PEER1_IP}:2380,peer-2=https://${PEER2_IP}:2380 \\

export ETCDCTL_API=3

etcdctl put name john

etcdctl get name

etcdctl get / --prefix --keys-only
````

#### Troubleshooting

````
kubectl logs web -f

kubectl logs web -f --previous

````

````
service kube-apiserver status

service kube-controller-manager status

service kube-scheduler status

service kubelet status // if on the control plane node(s)

service kube-proxy status // workers only

// check logs

kubectl logs kube-apiserver-master -n kube-system // if control-plane is deployed with pods, i.e. kubeadm

sudo journalctl -u kube-apiserver
````

````
kubectl describe node worker-1

service kubelet status

openssl x509 -in /var/lib/kubelet/worker-1.crt -text

cat /var/lib/kubelet/config.yaml

cat /etc/kubernetes/kubelet.conf


````

````
Network Plugin in kubernetes
--------------------

Kubernetes uses CNI plugins to setup network. The kubelet is responsible for executing plugins as we mention the following parameters in kubelet configuration.

- cni-bin-dir:  Kubelet probes this directory for plugins on startup

- network-plugin: The network plugin to use from cni-bin-dir. It must match the name reported by a plugin probed from the plugin directory.



There are several plugins available and these are some.



1. Weave Net:



 These is the only plugin mentioned in the kubernetes documentation. To install,



kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"



You can find this in following documentation :

         https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/



2. Flannel :



  To install,

  kubectl apply -f        https://raw.githubusercontent.com/coreos/flannel/2140ac876ef134e0ed5af15c65e414cf26827915/Documentation/kube-flannel.yml

  

Note: As of now flannel does not support kubernetes network policies.



3. Calico :

  

  To install,

  curl https://docs.projectcalico.org/manifests/calico.yaml -O

  Apply the manifest using the following command.

   kubectl apply -f calico.yaml



  Calico is said to have most advanced cni network plugin.



In CKA and CKAD exam, you won't be asked to install the cni plugin. But if asked you will be provided with the exact url to install it. If not, you can install weave net from the documentation 

   https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/high-availability/



Note: If there are multiple CNI configuration files in the directory, the kubelet uses the configuration file that comes first by name in lexicographic order.





DNS in Kubernetes
-----------------
Kubernetes uses CoreDNS. CoreDNS is a flexible, extensible DNS server that can serve as the Kubernetes cluster DNS.



Memory and Pods

In large scale Kubernetes clusters, CoreDNS's memory usage is predominantly affected by the number of Pods and Services in the cluster. Other factors include the size of the filled DNS answer cache, and the rate of queries received (QPS) per CoreDNS instance.



Kubernetes resources for coreDNS are:  

a service account named coredns,
cluster-roles named coredns and kube-dns
clusterrolebindings named coredns and kube-dns, 
a deployment named coredns,
a configmap named coredns and a
service named kube-dns.


While analyzing the coreDNS deployment you can see that the the Corefile plugin consists of important configuration which is defined as a configmap.



Port 53 is used for for DNS resolution.



    kubernetes cluster.local in-addr.arpa ip6.arpa {
       pods insecure
       fallthrough in-addr.arpa ip6.arpa
       ttl 30
    }


This is the backend to k8s for cluster.local and reverse domains.



proxy . /etc/resolv.conf



Forward out of cluster domains directly to right authoritative DNS server.





Troubleshooting issues related to coreDNS
1. If you find CoreDNS pods in pending state first check network plugin is installed.

2. coredns pods have CrashLoopBackOff or Error state

If you have nodes that are running SELinux with an older version of Docker you might experience a scenario where the coredns pods are not starting. To solve that you can try one of the following options:

a)Upgrade to a newer version of Docker.

b)Disable SELinux.

c)Modify the coredns deployment to set allowPrivilegeEscalation to true:



kubectl -n kube-system get deployment coredns -o yaml | \
  sed 's/allowPrivilegeEscalation: false/allowPrivilegeEscalation: true/g' | \
  kubectl apply -f -
d)Another cause for CoreDNS to have CrashLoopBackOff is when a CoreDNS Pod deployed in Kubernetes detects a loop.



 There are many ways to work around this issue, some are listed here:



Add the following to your kubelet config yaml: resolvConf: <path-to-your-real-resolv-conf-file> This flag tells kubelet to pass an alternate resolv.conf to Pods. For systems using systemd-resolved, /run/systemd/resolve/resolv.conf is typically the location of the "real" resolv.conf, although this can be different depending on your distribution.
Disable the local DNS cache on host nodes, and restore /etc/resolv.conf to the original.
A quick fix is to edit your Corefile, replacing forward . /etc/resolv.conf with the IP address of your upstream DNS, for example forward . 8.8.8.8. But this only fixes the issue for CoreDNS, kubelet will continue to forward the invalid resolv.conf to all default dnsPolicy Pods, leaving them unable to resolve DNS.

3. If CoreDNS pods and the kube-dns service is working fine, check the kube-dns service has valid endpoints.

        kubectl -n kube-system get ep kube-dns

If there are no endpoints for the service, inspect the service and make sure it uses the correct selectors and ports.





Kube-Proxy
---------
kube-proxy is a network proxy that runs on each node in the cluster. kube-proxy maintains network rules on nodes. These network rules allow network communication to the Pods from network sessions inside or outside of the cluster.



In a cluster configured with kubeadm, you can find kube-proxy as a daemonset.



kubeproxy is responsible for watching services and endpoint associated with each service. When the client is going to connect to the service using the virtual IP the kubeproxy is responsible for sending traffic to actual pods.



If you run a kubectl describe ds kube-proxy -n kube-system you can see that the kube-proxy binary runs with following command inside the kube-proxy container.



    Command:
      /usr/local/bin/kube-proxy
      --config=/var/lib/kube-proxy/config.conf
      --hostname-override=$(NODE_NAME)
 

  So it fetches the configuration from a configuration file ie, /var/lib/kube-proxy/config.conf and we can override the hostname with the node name of at which the pod is running.

 

 In the config file we define the clusterCIDR, kubeproxy mode, ipvs, iptables, bindaddress, kube-config etc.

 

Troubleshooting issues related to kube-proxy
1. Check kube-proxy pod in the kube-system namespace is running.

2. Check kube-proxy logs.

3. Check configmap is correctly defined and the config file for running kube-proxy binary is correct.

4. kube-config is defined in the config map.

5. check kube-proxy is running inside the container

# netstat -plan | grep kube-proxy
tcp        0      0 0.0.0.0:30081           0.0.0.0:*               LISTEN      1/kube-proxy
tcp        0      0 127.0.0.1:10249         0.0.0.0:*               LISTEN      1/kube-proxy
tcp        0      0 172.17.0.12:33706       172.17.0.12:6443        ESTABLISHED 1/kube-proxy
tcp6       0      0 :::10256                :::*                    LISTEN      1/kube-proxy




References:

Debug Service issues:

           https://kubernetes.io/docs/tasks/debug-application-cluster/debug-service/

DNS Troubleshooting:

           https://kubernetes.io/docs/tasks/administer-cluster/dns-debugging-resolution/

````

#### Intro YAML

````
Key Value Pair

Fruit: Apple
Vegetables: Carror


Array/Lists

Fruits:
- Orange
- Apple
- Banana

Vegetables:
- Carrot
- Tomato

Dictionary/Map

Banana:
  Calories: 105
  Fat: 0.4g
  Carbs: 27g

Grapes:
  Calories 62
  Fat: 0.3g
  Carbs: 16g
````

#### Dictionary - unordered List - ordered

````
employee:
    name: john
    gender: male
    age: 24
    
    address:
        city: edison
        state: new jersey
        country: united states

---
-
    name: apple
    color: red
    weight: 100g
- 
    name: apple
    color: red
    weight: 90g
- 
    name: mango
    color: yellow
    weight: 150g

---
# list of dictionaries

employees:
    -
        name: john
        gender: male
        age: 24

---

employees:
    -
        name: john
        gender: male
        age: 24
    -
        name: sarah
        gender: female
        age: 28

---

employee:
    name: john
    gender: male
    age: 24
    address:
        city: edison
        state: 'new jersey'
        country: 'united states'
    payslips:
        -
            month: june
            amount: 1400
        -
            month: july
            amount: 2400
        -
            month: august
            amount: 3400
````

#### JSON PATH is a query language for JSON

````
car.color

bus.price

vehicles.bus.price

$.vehicles.bus.price

// all output from JSON PATH is an array(or lists for Python folks)

$[0] - Output would be car here
$[0,3] - Get the first and 4th items

$.car.wheels[1].model

$[?(@ > 40)] - Get all items greater than 40

@ == 40
@ != 40
@ in [40,43,45]
@ nin [40,43,35]

$.car.wheels[?(@.location == "rear-right")].model

````

#### Wildcards in JSON PATH Queries

````
$.*.color

$.*.price

$.[*].model

$.car.wheels[*].model
$.bus.wheels[*].model

$.*.wheels[*].model


$.prizes.[?(@.year == "2014")].laureates[*].firstname - Find all the firstnames of all winners in year 2014
````

#### JSON PATH Lists

````
$[0:4] - get the first 4 elements

$[0:8:2] - skip over 2

$[9]

$[-1]
$[-1:0]
$[-1:]

$[-3:]

$[-5:].age - age of the last 5 users
````

#### JSON PATH in kubectl

````
kubectl get nodes -o json

kubectl get pods -o json

.items[0].spec.containers[0].image // $ not mandatory kubectl adds it for you
kubectl get pods -o=jsonpath=.'{items[0].spec.containers[0].image}'

kubectl get pods -n dev-apps -o=jsonpath='{.items[*].spec.containers[0].image}'

kubectl get nodes -o=jsonpath='{.items[*].metadata.name}'

kubectl get nodes -o=jsonpath='{.items[*].status.nodeInfo.architecture}'

kubectl get nodes -o=jsonpath='{.items[*].status.capacity.cpu}'

kubectl get nodes -o=jsonpath='{.items[*].metadata.name}{.items[*].status.capacity.cpu}'

kubectl get nodes -o=jsonpath='{.items[*].metadata.name} {"\t"} {.items[*].status.capacity.cpu}{"\n"}'

kubectl get nodes -o=jsonpath='{range .items[*]}{.metadata.name}{"\t"}{.status.capacity.cpu}{"\n"}{end}'


kubectl get nodes -o=custom-columns=NODE:.metadata.name

kubectl get nodes -o=custom-columns=NODE:.metadata.name,CPU:.status.capacity.cpu

kubectl get nodes --sort-by=.metadata.name

kubectl get nodes --sort-by=.status.capacity.cpu


$.status.containerStatuses[?(@.name == "redis-container")].restartCount

$.[*].metadata.name

$.users[*].name

kubectl config view --kubeconfig=my-kube-config -o jsonpath="{.users[*].name}" > /opt/outputs/users.txt

kubectl get pv -o=jsonpath='{.items[*].spec.capacity.storage}'

kubectl get pv --sort-by=.spec.capacity.storage

kubectl get pv -o=custom-columns=NAME:.metadata.name,CAPACITY:.spec.capacity.storage --sort-by=.spec.capacity.storage

kubectl config view --kubeconfig=my-kube-config -o=jsonpath='{.contexts[?(@.cont/aws-context-nameuser")].context}' > /opt/outputs/aws-context-name

kubectl config view --kubeconfig=my-kube-config -o jsonpath="{.contexts[?(@.context.user=='aws-user')].name}" > /opt/outputs/aws-context-name

````

#### EXAM sample questions

````
kubectl apply -f deploy.yaml --record

kubectl rollout history deployment nginx-deploy

kubectl set image deployment/nginx-deploy nginx=nginx:1.17 --record

kubectl rollout history deployment nginx-deploy

kubectl create role developer --resource=pods --verb=create,list,get,update,delete --namespace=development

kubectl create rolebinding developer-role-binding --role=developer --user=john --namespace=development

kubectl auth can-i update pods --as=john --namespace=development

kubectl expose pod nginx-resolver --name=nginx-resolver-service --port=80 --target-port=80 --type=ClusterIP

kubectl run test-nslookup --image=busybox:1.28 --rm -it --restart=Never -- nslookup nginx-resolver-service
kubectl run test-nslookup --image=busybox:1.28 --rm -it --restart=Never -- nslookup nginx-resolver-service > /root/CKA/nginx.svc

kubectl get pod nginx-resolver -o wide
kubectl run test-nslookup --image=busybox:1.28 --rm -it --restart=Never -- nslookup <P-O-D-I-P.default.pod> > /root/CKA/nginx.pod

kubectl get nodes -o jsonpath='{.items[*].status.addresses[?(@.type=="InternalIP")].address}'

---
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: ingress-to-nptest
  namespace: default
spec:
  podSelector:
    matchLabels:
      run: np-test-1
  policyTypes:
  - Ingress
  ingress:
  - ports:
    - protocol: TCP
      port: 80



kubectl taint node node01 env_type=production:NoSchedule

kubectl run dev-redis --image=redis:alpine

---
apiVersion: v1
kind: Pod
metadata:
  name: prod-redis
spec:
  containers:
  - name: prod-redis
    image: redis:alpine
  tolerations:
  - effect: NoSchedule
    key: env_type
    operator: Equal
    value: production


kubectl create namespace hr

kubectl run hr-pod --image=redis:alpine --namespace=hr --labels=environment=production,tier=frontend

````


```SAMPLE CHALLENGES```


````

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  # "namespace" omitted since ClusterRoles are not namespaced
  name: pvviewer-role
rules:
- apiGroups: [""]
  #
  # at the HTTP level, the name of the resource for accessing Secret
  # objects is "secrets"
  resources: ["persistentvolumes"]
  verbs: ["list"]

````


````

apiVersion: rbac.authorization.k8s.io/v1
# This cluster role binding allows anyone in the "manager" group to read secrets in any namespace.
kind: ClusterRoleBinding
metadata:
  name: pvviewer-role-binding
subjects:
- kind: ServiceAccount
  name: pvviewer # Name is case sensitive
  #apiGroup: rbac.authorization.k8s.io #I had to comment this out so it worked, odd 1.20 shows you can use it
  namespace: default #namespace where the account is is required
roleRef:
  kind: ClusterRole
  name: pvviewer-role
  apiGroup: rbac.authorization.k8s.io

````



````

apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: test-network-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      run: np-test1
  policyTypes:
    - Ingress
  ingress:
    - #allow any pod to communicate over tcp 80 to po matching run: nptest1
      ports:
        - protocol: TCP
          port: 80

````


````

kubectl get nodes -o=jsonpath="{.items[*].status.addresses[?(@.type=='InternalIP')].address}"

````

````
kubectl get nodes -o=custom-columns=OSVERSION:.status.nodeInfo.osImage,OS:.status.nodeInfo.operatingSystem

````


````

kubectl run curl --image=alpine/curl --rm -it -- sh

````
