```CKS notes```


```4C's of CloudNative Security```

1. Cloud
2. Cluster
3. Container
4. Code


```CIS Benchmarks```

#### Center for Internet Security, a non-profit entity

#### CIS-Cat Tools automated tool to run CIS Benchmarks against your systems and generate an HTML report


#### Run the command in interactive mode and generate a file in /var/www/html with the name index.html

````

./Assessor-CLI.sh -i -rd /var/www/html/ -nts -rp index

````


```kube-bench```


````

./kube-bench --config-dir `pwd`/cfg --config `pwd`/cfg/config.yaml

````

```Authentication```

1. Admins - Users
2. Developers - Users
3. Bots - Service Accounts

#### For user Authentication we can use

1. Static Password files
2. Static Token files
3. Certificates
4. Identity Services(LDAPS, OAuth, etc.)

#### kube-apiserver.service 

````
--basic-auth-file=user-details.csv
rest of options

````

#### Restart service

#### If you used the kubeadm tool then you need to add the above option to /etc/kubernetes/manifests/kube-apiserver.yaml


````
--token-auth-file=user-token-details.csv
rest of options

````

```ServiceAccounts```

````

kubectl create sa dashboard-sa

````


#### Each service account that gets created a token is automatically generated as well, that token is used to authenticate to the kube-api server

````
---
apiVersion: v1
kind: Pod 
metadata:
  name: pod-with-custom-sa 
  labels:
    env: dev
  namespace: default

spec:
  containers:
    - name: pod
      image: myimage:v1
    serviceAccountName: dashboard-sa
    automountServiceAccountToken: false #choose to not auto mount the default token/sa if you want

````


#### Changes to SA as of k8s 1.22/1.24

#### In 1.22 the TokenRequestAPI was proposed to make it more secure and scaleable
#### Major advatanges are tokens from the API are 

1. Audience bound
2. Time bound
3. Object bound


#### In 1.24 when you create a SA it no longer creates a secret with the token in it so you will need to run the below as of 1.24

````
kubectl create sa dashboard-sa

kubectl create token dashboard-sa #default expiry here is 1 hour need to pass args to make longer

````

#### If you would like to create a token the old way, first create the SA and then 

````
---
apiVersion: v1
kind: Secret
type: kubernetes.io/service-account-token
metadata:
  name: my-token
  annotations:
    kubernetes.io/service-account.name: dashboard-sa
````


```TLS```

#### With Symmetric encryption the both the public and private keys must be sent over the network, which is unsafe

#### Asymmetric Encryption uses both a private and a public key the Symmetric key is encrypted with the servers public key and sent over the network then the servers private key decrypts the Symmetric key, the hacker does not have the servers private key so they can't decrypt the Symmetric key


#### Tools to generate certs include easyrsa, openssl, and cfssl

````

openssl genrsa -out ca.key 2048

openssl req -new -key ca.key -subj "/CN=KUBERNETES-CA" -out ca.csr

openssl x509 -req -in ca.csr -signkey ca.key -out ca.crt

##generate client certs

openssl genrsa -out admin.key 2048

openssl req -new -key admin.key -subj "/CN=kube-admin/O=system:masters" -out admin.csr 

openssl x509 -req -in admin.csr -CA ca.crt -CAkey ca.key -out admin.crt

````

#### When creating the cert for scheduler, controller-manager,  it must be prefixed with the keyword SYSTEM so SYSTEM:KUBE-SCHEDULER


````

curl https://kube-apiserver:6443/api/v1/pods --key admin.key --cert admin.crt --cacert ca.crt

````

#### For the kube-api server you need to create an openssl.cnf file so you can add Subject Alternative Names to the cert

#### For kubelet the cert name should be the name of the node(s)

#### The kubelet cert to communicate with the kube-api server must use the names as SYSTEM:node:node01 etc. and they must have a group of SYSTEM:NODES


````

openssl x509 -in /etc/kubernetes/pki/apiserver.crt -text -noout

journalctl -u etcd.service -l

kubectl log etcd-master  ##etcd running as a pod

docker logs aec #one layer down check the pod logs with docker, we can't do this going forward as docker is deprecated so we need to use containerd or cri-o log commands

ctr --help #containerd equivalent

````

[https://iximiuz.com/en/posts/containerd-command-line-clients/](https://iximiuz.com/en/posts/containerd-command-line-clients/)

[https://platform9.com/docs/kubernetes/containerd-commands-and-info](https://platform9.com/docs/kubernetes/containerd-commands-and-info)



#### If you are using containerd for your runtime hopefully crictl is also installed

````
which crictl

crictl ps -a #list everything, use this

crictl pods #list pods


crictl logs aqwsed

````


```Certificates API```

````
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: jabe
spec:
  groups:
  - system:authenticated
  usages:
  - client auth
  request: base64-encoded-csr
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 864000 #10 days this should work from 1.22+

````


````


cat myuser.csr | base64 | tr -d "\n"

kubectl get csr

kubectl certificate approve jane

kubectl certificate deny jane

kubectl delete csr agent-smith

kubectl get csr jane -oyaml

kubectl get csr jane -ojsonpath='{.status.certificate}' | base64 -d > jane.crt

````

#### Controller-Manager does all certificate related work



```kubeconfig```


````

kubectl get pods --server my_server:6443 --client-key=admin.key --client-certificate=admin.crt --certificate-authority=ca.crt

kubectl get pods --kubeconfig=my-config-file

````

#### kubeconfig has 3 sections, Clusters, Users, and Contexts

#### Contexts marry the 2 users, to clusters Admin@production connect the Admins user to the production cluster


````
apiVersion: v1
kind: Config

current-context: my-kube-admin@my-kube-playground

clusters:
- name: my-kube-playground
  cluster:
    certificate-authority: ca.crt
    #certificate-authority-data: base64 encoded value here
    server: https://my-kube-playground:6443

contexts:
- name: my-kube-admin@my-kube-playground
  context:
    cluster: my-kube-playground
    user: my-kube-admin
    namespace: finance #optional

users:
- name: my-kube-admin
  user:
    client-certificate: admin.crt
    client-key: admin.key


````

````

kubectl config view

````


```API Groups```


````

/metrics
/healthz

/api  #core group(legacy)

/apis  #named groups this endpoint is the future

````

[https://kubernetes.io/docs/reference/using-api/](https://kubernetes.io/docs/reference/using-api/)


#### Instead of using curl you can use a proxy


````

kubectl proxy 

#then

curl http://localhost:8001 -k

````


```Authorization```

### Node, ABAC, RBAC, Webhook

#### kubelet, kube-api uses Node Authorizer

### ABAC users can have this type this requires a reboot of the kube-API server everytime it Changes

#### RBAC fixes the above limitations

#### Webhook is for external access pinniped, OPA, etc.. is an exanple of this

#### Always Allow, Always Deny 2 other authorization modes


```kube-api server config examples```

````

--authorization-mode=AlwaysAllow #default if you don't set it

#or

--authorization-mode=Node,RBAC,Webhook #Authorization in this order

````

```RBAC```


````
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer 

rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]
- apiGroups: [""]
  resources: ["ConfigMaps"]
  verbs: ["create"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: devuser-developer-binding 

subjects:
- kind: User 
  name: dev-user 
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: Role
  name: developer
  apiGroup: rbac.authorization.k8s.io

---

apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer
  namespace: test

rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]
- apiGroups: [""]
  resources: ["ConfigMaps"]
  verbs: ["create"]
  resourceNames: ["blue", "orange"]

---

apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: developer
  namespace: test

rules:
- apiGroups: [""]
  resources: ["pods"]
  verbs: ["list", "get", "create", "update", "delete"]
- apiGroups: [""]
  resources: ["ConfigMaps"]
  verbs: ["create"]
- apiGroups: ["apps"]
  resources: ["deployments"]
  verbs: ["create"]
````

````

kubectl auth can-i create deployments

kubectl auth can-i create deployments --as dev-user

````

[https://kubernetes.io/docs/reference/access-authn-authz/rbac/](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)


```ClusterRoles & ClustgerRolebinding```


````
kubectl api-resources --namespaced=false
````


````
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: cluster-administrator

rules:
- apiGroups: [""]
  resources: ["nodes"]
  verbs: ["list", "get", "create", "update", "delete"]

---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: cluster-admin-role-binding 

subjects:
- kind: User 
  name: cluster-admin 
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole
  name: cluster-administrator
  apiGroup: rbac.authorization.k8s.io


````

```kubelet security```

#### Prior to k8s 1.10 a lot of the config was stored in the kubelet.service file after 1.10 it was moved to kubelet-config.yaml or it can just be called config.yaml


````
ps -aux | grep -i kubelet

curl -sk https://localhost:10250/pods/

curl -sk https://localhost:10250/logs/syslog

curl -sk https://localhost:10255/metrics

````

#### Ports 10250 - full api here , 10255 - servers api that allows unauthenticated read-only access

#### We can set auth in 2 places for the kubelet

1. kubelet.service
2. kubelet-config.yaml

#### 1. --anonymous-auth=false

````
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletCOnfiguration
authentication:
  anonymous:
    enabled: false

````

#### 2 methods of authentication

1. Certificates x509**
2. API Bearer Tokens


````

--client-ca-file=/path/tp/ca.crt

````


````

apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
authentication:
  x509:
    clientCAFile: /path/to/ca.crt
---

curl -sk https://localhost:10250/pods/ --key kubelet-key.pem --cert kubelet-cert.pem
````

#### kube-api server must also have a key-pair to talk to the kubelet

````
--kubelet-client-certificate=/path/to/kubelet-cert.pem
--kubelet-client-key=/path/to/kubelet-key.pem
````


```Authorization```


#### Default is always allow 

````
##kubelet.service
--authorization-mode=Webhook

---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
authorization:
  mode: Webhook

````


```Metrics Server```

#### read-only access is on port 10255 by default

````
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
readOnlyPort: 0 #disabled read-only access

---
apiVersion: kubelet.config.k8s.io/v1beta1
kind: KubeletConfiguration
authentication:
  anonymous:
    enabled: false
  x509:
    clientCAFile: /path/to/ca.crt
authorization:
  mode: Webhook
readOnlyPort: 0 #disabled read-only access


````

```kubectl proxy & port forward```


````
kubectl proxy  # start a proxy on port 8001 to the kube-api server

kubectl proxy --port=8002

curl http://localhost:8001 -k

curl http://localhost:8001/api/v1/namespaces/default/services/nginx/proxy/

kubectl port-forward service/nginx 28080:80

````

```kubernetes dashboard```

[https://redlock.io/blog/cryptojacking-tesla](https://redlock.io/blog/cryptojacking-tesla)

[https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)

[https://github.com/kubernetes/dashboard](https://github.com/kubernetes/dashboard)

[https://www.youtube.com/watch?v=od8TnIvuADg](https://www.youtube.com/watch?v=od8TnIvuADg)

[https://blog.heptio.com/on-securing-the-kubernetes-dashboard-16b09b1b7aca](https://blog.heptio.com/on-securing-the-kubernetes-dashboard-16b09b1b7aca)

[https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)


````
cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: dashboard-admin-binding
  namespace: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: admin
subjects:
- kind: ServiceAccount
  name: dashboard-admin
  namespace: kubernetes-dashboard
EOF

---

cat <<EOF | kubectl apply -f -
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: dashboard-admin-list-namespace-binding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: list-namespace
subjects:
- kind: ServiceAccount
  name: dashboard-admin
  namespace: kubernetes-dashboard
EOF

````

```Verify platform binaries```


````

shasum -a 512 kuberntes.tar.gz  #MAC

sha512sum kuberntes.tar.gz  #Linux

tar -zcvf kubernetes.tar.gz kubernetes

````

[https://github.com/kubernetes/kubernetes/tree/master/CHANGELOG](https://github.com/kubernetes/kubernetes/tree/master/CHANGELOG)



```Cluster upgrade```


##### kube-api must be at the highest version in the cluster

##### controller-manager and kube-scheduler can be 1 version behind, kubectl can be 1 version ahead or 1 behind 

##### kubelet and kubeproxy can be 2 versions behind

##### kubernetes only supports 3 minor versions, upgrade 1 minor version at a time 


```Upgrade control-plane```


````

kubeadm upgrade plan 

sudo apt-cache madison kubeadm 

sudo apt upgrade -y kubeadm=1.18.0-00

kubeadm upgrade apply v1.18.0  #Or whatever version

kubectl drain node-01

sudo apt upgrade kubelet=1.18.0-00

sudo systemctl daemon-reload
sudo systemct restart kubelet

kubectl uncordon controlplane

````

```Upgrade workers```


````

kubectl drain node-01  ## On controller


sudo apt upgrade -y kubeadm=1.18.0-00

kubeadm upgrade node ## This is the command on the officially k8s docs 

#kubeadm upgrade node config --kubelet-version=v1.18.0

sudo apt upgrade kubelet=1.18.0-00

sudo systemctl daemon-reload
sudo systemctl restart kubelet

kubectl uncordon node-1  ## On controller

````

```Network Policy```


````
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy 
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          name: api-pod
    ports:
    - protocol: TCP
      port: 3306

---

apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: db-policy 
  namespace: prod
spec:
  podSelector:
    matchLabels:
      role: db
  policyTypes:
  - Ingress
  - Egress
  ingress:
  - from:
    - podSelector:
        matchLabels:         ##these 2 are && rules both these have to match
          name: api-pod
      namespaceSelector:
        matchLabels:
          name: staging
    #- namespaceSelector:  ##If we do this and comment out the above namespaceSelector then this would also be an OR
        #matchLabels:
          #name: staging
      
    - ipBlock:   ##This is and OR rule
        cidr: 192.168.5.10/32  
    ports:
    - protocol: TCP
      port: 3306
  egress:
  - to:
    - ipBlock:
        cidr: 192.168.6.0/24
    ports:
    - protocol: TCP
      port: 1234

````


````

apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: internal-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      name: internal
  policyTypes:
  - Egress
  - Ingress
  ingress:
    - {}
  egress:
  - to:
    - podSelector:
        matchLabels:
          name: mysql
    ports:
    - protocol: TCP
      port: 3306

  - to:
    - podSelector:
        matchLabels:
          name: payroll
    ports:
    - protocol: TCP
      port: 8080

  - ports:   ## Allow egress out to DNS note instead of using -to we are just using -ports:
    - port: 53
      protocol: UDP
    - port: 53
      protocol: TCP

````


```Ingress```

````
apiVersion: apps/v1
kind: Deployment 
matadata:
  name: nginx-ingress-controller 
spec:
  replicas: 1
  selector:
    matchLabels:
      name: nginx-ingress
  template:
    metadata:
      labels:
        name: nginx-ingress
    spec:
      containers:
        - name: nginx-ingress-controller
          image: quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.21.0
      args:
      - /nginx-ingress-controller
      - --configmap=$(POD_NAMESPACE)/nginx-configuration
      env:
        - name: POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name 
        - name: POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
      ports:
        - name: http
          containerPort: 80
        - name: https
          containerPort: 443

````

````
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: minimal-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80
````


[https://kubernetes.github.io/ingress-nginx/examples/rewrite/](https://kubernetes.github.io/ingress-nginx/examples/rewrite/)

```Imperative command```

````

kubectl create ingress ingress-test --rule="wear.my-online-store.com/wear*=wear-service:80"

````

[https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-em-ingress-em- ](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#-em-ingress-em-)


[https://kubernetes.io/docs/concepts/services-networking/ingress](https://kubernetes.io/docs/concepts/services-networking/ingress)

[https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types](https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types)


```Ingress – Annotations and rewrite-target```

#### Different ingress controllers have different options that can be used to customise the way it works. NGINX Ingress controller has many options that can be seen [here](https://kubernetes.github.io/ingress-nginx/examples/). I would like to explain one such option that we will use in our labs. The [Rewrite](https://kubernetes.github.io/ingress-nginx/examples/rewrite/) target option.

````
Our watch app displays the video streaming webpage at http://<watch-service>:<port>/

Our wear app displays the apparel webpage at http://<wear-service>:<port>/
````

#### We must configure Ingress to achieve the below. When user visits the URL on the left, his request should be forwarded internally to the URL on the right. Note that the /watch and /wear URL path are what we configure on the ingress controller so we can forwarded users to the appropriate application in the backend. The applications don’t have this URL/Path configured on them:

````
http://<ingress-service>:<ingress-port>/watch –> http://<watch-service>:<port>/

http://<ingress-service>:<ingress-port>/wear –> http://<wear-service>:<port>/

Without the rewrite-target option, this is what would happen:

http://<ingress-service>:<ingress-port>/watch –> http://<watch-service>:<port>/watch

http://<ingress-service>:<ingress-port>/wear –> http://<wear-service>:<port>/wear

````

#### Notice watch and wear at the end of the target URLs. The target applications are not configured with /watch or /wear paths. They are different applications built specifically for their purpose, so they don’t expect /watch or /wear in the URLs. And as such the requests would fail and throw a 404 not found error.

#### To fix that we want to “ReWrite” the URL when the request is passed on to the watch or wear applications. We don’t want to pass in the same path that user typed in. So we specify the rewrite-target option. This rewrites the URL by replacing whatever is under rules->http->paths->path which happens to be /pay in this case with the value in rewrite-target. This works just like a search and replace function.

````
For example: replace(path, rewrite-target)

In our case: replace("/path","/")

````

````
apiVersion: networking.k8s.io/v1 
kind: Ingress
metadata:
  name: test-ingress
  namespace: critical-space
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /pay
        pathType: Prefix
        backend:
          service:
            name: pay-service
            port: 
              number: 8282

````

#### In another example given [here](https://kubernetes.github.io/ingress-nginx/examples/rewrite/), this could also be:

````

replace("/something(/|$)(.*)", "/$2")

````


````

apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
  name: rewrite
  namespace: default
spec:
  rules:
  - host: rewrite.bar.com
    http:
      paths:
      - backend:
         service:
          name: http-svc
          port: 
           number: 80
        path: /something(/|$)(.*)
        pathType: Prefix


````


````

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ingress-controller
  namespace: ingress-space
spec:
  replicas: 1
  selector:
    matchLabels:
      name: ingress-controller
  template:
    metadata:
      labels:
        name: ingress-controller
    spec:
      serviceAccountName: ingress-serviceaccount
      containers:
        - name: nginx-ingress-controller
          image: quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.21.0
          args:
            - /nginx-ingress-controller
            - --configmap=$(POD_NAMESPACE)/nginx-configuration
            - --default-backend-service=app-space/default-http-backend
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          ports:
            - name: http
              containerPort: 80
            - name: https
              containerPort: 443

````

````
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-wear-watch
  namespace: app-space
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  rules:
  - http:
      paths:
      - path: /wear
        pathType: Prefix
        backend:
          service:
           name: wear-service
           port: 
            number: 8080
      - path: /watch
        pathType: Prefix
        backend:
          service:
           name: video-service
           port:
            number: 8080

````


```Docker Service Config```


````
dockerd

dockerd --debug

/var/run/docker.sock

dockerd --debug --host=tcp://192.168.1.10:2375

export DOCKER_HOST="tcp://192.168.1.10:2375"

docker ps ## this will now show processes running on the external host

## enable TLS

dockerd --debug --host=tcp://192.168.1.10:2376 --tls=true --tlscert=/var/docker/server.pem --tlskey=/var/docker/serverkey.pem

export DOCKER_HOST="tcp://192.168.1.10:2376"
export DOCKER_TLS=true
export DOCKER_TLS_VERIFY=true

docker --tlscert="path_to_cert" --tlskey="path_to_key" --tlscacert="pat_to_ca_cert" ps -a

````

##### Instead of the above we can move all this to a config file @ /etc/docker/daemon.json


```daemon.json```

````

{
  "debug": true,
  "hosts": ["tcp://192.168.1.10:2376"],
  "tls": true,
  "tlscert": "/var/docker/server.pem",
  "tlskey": "/var/docker/serverkey.pem",
  "tlsverify": true,
  "tlscacert": "/var/docker/caserver.pem"
}

````

```Secure Docker Daemon```

##### Or you can drop the certs in ~/.docker directory for the user and then youcan just run docker ps -a


```Least Privilege Principle```

1. User Accounts - ex. Bob, Mike, etc.
2. Superuser Account - ex. root 
3. System Accounts - ex. ssh, mail, etc.
4. Service Accounts - ex. nginx, http, etc.

##### super user is root UID=0


````

id
id -u david 

who 

last 

````


1. /etc/passwd 
2. /etc/shadow
3. /etc/group


````

usermod -s /bin/nologin michael  #disable michael's account

userdel bob  #delete bob's account

deluser michael admin  #remove michael from the admin group

groupdel devs #delete group devs

adduser --home /opt/sam --shell /bin/bash --uid 2328 sam
usermod -aG admin sam

````

```SSH Hardening```

````

ssh-keygen -t rsa 

ssh-copy-id mark@node01 

cat ~/.ssh/authorized_keys

vi /etc/ssh/sshd_config 

PermitRootLogin no 
PasswordAuthentication no

````

[https://www.cisecurity.org/cis-benchmarks/](https://www.cisecurity.org/cis-benchmarks/)


````
Go to the `Operating Systems` section and search for the `Distribution Independent Linux`. Expand it to see more options then download CIS Benchmark.

````

```Privileged Escalation in Linux```

````
visudo

cat /etc/sudoers

ssh-copy-id -i ~/.ssh/id_rsa.pub jim@node01

jim ALL=(ALL) NOPASSWD:ALL  #allow Jim to run sudo without entering a password 

%admin ALL=(ALL) NOPASSWD:ALL  #any member of the admin group can sudo without using a password

````

```Remove Obsolete Packages and Services```


````
systemctl list-units --type service

systemctl stop apache2

systemctl disable apache2

apt remove apache2

apt list --installed

apt show wget #this sees what is out there not what's on the system

apt-cache madison wget

apt list --installed | grep -i wget  #this tells us what version is on the system now

rm -f /lib/systemd/system/nginx.service  #remove the nginx unit file
````

```Restrict Kernel Modules```

````
modprobe pcspkr

lsmod

cat /etc/modprobe.d/blacklist.conf 

vi /etc/modprobe.d/blacklist.conf

blacklist sctp  ## add this to the above file note the file can be called anything
blacklist dccp

shutdown -r now

lsmod | grep -i sctp

````

```Identify open ports```


````

netstat -an | grep -w LISTEN

cat /etc/services | grep -w 53  #ubuntu systems only, probably Debian too??

netstat -natp | grep 9090

````


##### Below are some references:


[https://access.redhat.com/security/cve/cve-2019-3874](https://access.redhat.com/security/cve/cve-2019-3874)

[https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/#check-required-ports)



```Minimize IAM Role```

#### IAM role is created so an EC2 instance can talk to s3 or similar

#### AWS trusted advisor, Security Command Center(GCP), Azure Advisor


```Minimize external access to the network```

```UFW - Uncomplicated firewall```


````
netstat -an | grep -w LISTEN

apt-get update

apt-get install -y ufw 

ufw status

ufw default allow outgoing

ufw default deny incoming

ufw allow from 172.16.238.5 to any port 22 proto tcp

ufw allow from 172.16.238.5 to any port 80 proto tcp 

ufw allow from 172.16.100.0/28 to any port 80 tcp

ufw allow from any to any port 22 proto tcp

ufw allow from 135.22.65.0/24 to any port 9090 proto tcp

ufw allow 6000:6500/tcp

ufw deny 8080

ufw deny 389/tcp

ufw deny from 192.168.0.150 to any port 8000:8200/tcp

ufw enable 

ufw status

ufw status numbered

ufw reset 

ufw 

ufw delete deny 8080

ufw delete 5

ufw disable 

````

```Linux syscalls```


1. Kernel space
2. User space

#### C, Java, Python, Ruby, Containers run in user space

#### Kernel code, Kernel Extensions, Device Drivers, run in kernel space

#### Apps make system calls(sys calls)

````

which strace

strace touch /tmp/error.log

env | wc -l

pidof etcd 

strace -p 3596

strace -c touch /tmp/error.log

````

```Aquasec tracee```

#### Uses ebpf and it runs in Kernel space

1. /tmp/tracee - Default workspace
2. /lib/modules - Kernel Headers
3. /usr/src - Kernel Headers


````

docker run --name tracee --rm --privileged --pid=host -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro \
-v /tmp/tracee:/tmp/tracee aquasec/tracee:0.4.0 --trace comm=ls


docker run --name tracee --rm --privileged --pid=host -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro \
-v /tmp/tracee:/tmp/tracee aquasec/tracee:0.4.0 --trace pid=new  ##All new processes

docker run --name tracee --rm --privileged --pid=host -v /lib/modules/:/lib/modules/:ro -v /usr/src:/usr/src:ro \
-v /tmp/tracee:/tmp/tracee aquasec/tracee:0.4.0 --trace container=new  ##All new containers
````

```Restrict Syscalls```

#### Docker/OCI enables SYSCALL by Default


````

grep -i seccomp /boot/config-$(uname -r)

grep Seccomp /proc/1/status


docker run -it --rm --security-opt seccomp=/root/custom.json \  ## use a custom seccomp profile
docker/whalesay /bin/sh

docker run -it --rm --security-opt seccomp=unconfined docker/whalesay /bin/bash ##turn off seccomp
````

#### Below is an example of a whitelist

```custom.json```

````
{
  "defaultActions": "SCMP_ACT_ERRNO",
  "architecture": [
    "SCMP_ARCH_X86_64",
    "SCMP_ARCH_X86",
    "SCMP_ARCH_X32"
  ],
  "syscalls": [
    {
      "names": [
        "arch_prctl",
        "brk",
        "capget",
        "capset",
        "close",
        "execve",
        "clone"
      ],
      "action": "SCMP_ACT_ALLOW"
    }
  ]
}

````

#### Seccomp modes

1. Mode-0 - disabled
2. Mode-1 - STRICT
3. Mode-2 - FILTERED


```Seccomp in k8s```


````
docker run r.j3ss.co/amicontained amicontained

kubetl run amicontained --image=r.j3ss.co/amicontained amicontained -- amicontained 

kubectl logs amicontained

````

#### By default as of k8s 1.20 seccomp is not on by default you must define it in your manifest file

````
---
apiVersion: v1 
kind: Pod
metadata:
  labels:
    run: amicontained
  name: amicontained
spec:
  securityContext:
    seccompProfile:
      type: RuntimeDefault
  containers:
  - args:
    - amicontained
    image: r.j3ss.co/amicontained
    name: amicontained
    securityContext:
      allowPrivilegeEscalation: false

---
---
apiVersion: v1 
kind: Pod
metadata:
  labels:
    run: amicontained
  name: amicontained
spec:
  securityContext:
    seccompProfile:
      type: Unconfined  #This is the default behavior
  containers:
  - args:
    - amicontained
    image: r.j3ss.co/amicontained
    name: amicontained
    securityContext:
      allowPrivilegeEscalation: false

---
apiVersion: v1 
kind: Pod
metadata:
  name: test-audit
spec:
  securityContext:
    seccompProfile:
      type: Localhost
      localhostProfile: profiles/audit.json #relative to /var/lib/kubelet/seccomp
  containers:
  - command: ["bash", "-c", "echo 'I just makde some syscalls' && sleep 100"]
    image: ubuntu
    name: ubuntu
    securityContext:
      allowPrivilegeEscalation: false

````

````

mkdir -p /var/lib/kubelet/seccomp/profiles

touch /var/lib/kubelet/seccomp/profiles/audit.json

grep syscall /var/log/syslog

grep -w 35 /usr/include/asm/unistd_64.h  #ubuntu, maybe debian as well
````

```audit.json```

````
{
  "defaultAction": "SCMP_ACT_LOG" #log everything
}

````


```violation.json```

````
{
  "defaultAction": "SCMP_ACT_ERRNO" #deny everything
}

````

```AppArmor```


````

systemctl status apparmor 

cat /sys/module/apparmor/parameters/enabled ##Should have a 'Y'

cat /sys/kernel/security/apparmor/profiles

````

```apparmor-deny-write```

````
profile apparmor-deny-write flags=(attach_disconnected) {
  file,
  # Deny all file writes
  deny /** w,
}

````

```apparmor-deny-proc-write```


````
profile apparmor-deny-proc-write flags=(attach_disconnected) {
  file,
  # Deny all file writes
  deny /proc/* w,
}

````


```apparmor-deny-remount-root```


````
profile apparmor-deny-remount-root flags=(attach_disconnected) {
  file,
  # Deny remount readonly the root filesystem
  deny mount options=(ro,remount) -> /,
}

````

````
aa-status

````

#### AppArmor rules can be loaded in 3 modes 

1. enforce
2. complain - logging only
3. unconfined

````

apt-get install -y apparmor-utils

aa-genprof /root/add_data.sh  ##our script

### Now on a separate window run the script so the tool and figure out what it's doing > Press 'S' to scan > Press 'I' > Press 'I' again, Sev 10 is the highest > Press 'A' > Press 'D' > Press 'S' to save then 'F' to finish

cat /etc/apparmor.d/root.add_data.sh

apparmor_parser /etc/apparmor.d/root.add_data.sh #load our profile, do this on each node in the cluster

#disable 
apparmor_parser -R /etc/apparmor.d/root.add_data.sh

ln -s /etc/apparmor.d/root.add_data.sh /etc/apparmor.d/disable

````


```AppArmor K8s```

##### Added in k8s 1.4 as of 1.20 still in beta

1. AppArmor Kernel modules Enabled on all nodes 
2. AppArmor Profile loaded in the kernel
3. Container runtime should support it


````
---
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
  annotations:
    container.apparmor.security.beta.kubernetes.io/ubuntu-sleeper: localhost/apparmor-deny-write

spec:
  containers:
  - name: hello
    image: ubuntu
    command: ["sh", "-c", "echo 'Sleeping for an hour!' && sleep 1h"]


````


````
aa-status # run on all nodes to make sure the apparmor-deny-write pofile is loaded or whatever profile you created is loaded

````

```Linux capabilities```


##### In Linux Kernel < 2.2 processes ran as 2 types

1. Priviliged process
2. Unpriviliged process


##### In Linux Kernel >= 2.2 they added capabilities

#### Example 

1. CAP_CHOWN
2. CAP_SYS_TIME
3. CAP_NET_ADMIN
4. CAP_SYS_BOOT


````

getcap /usr/bin/ping

ps -ef | gre /usr/sbin/sshd | grep -v grep  #get pid

getpcaps 779

````


````
---
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
spec:
  containers:
  - name: ubuntu-sleeper
    image: ubuntu
    command: ["sleep", "1000"]
    securityContext:
      capabilities:
        add: ["SYS_TIME"]
        drop: ["CHOWN"]


````


```Security Context```



````
---
apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
spec:
  securityContext:
    runAsUser: 1000 #Applies to the container(s)
  containers:
  - name: ubuntu-sleeper
    image: ubuntu
    command: ["sleep", "1000"]
    securityContext:
      runAsUser: 1000  #Applies to this pod only ovverides the container securityContext
      capabilities:
        add: ["SYS_TIME", "NET_ADMIN"]
        #drop: ["CHOWN"]


````

```Admission Controllers```

1. AlwaysPullImages
2. DefaultStorageClass
3. EventRateLimit
4. NameSpaceExists - Depracted replaced with NamespaceLifecycle controller
5. NamespaceAutoProvision - not enabled by default will create ns if it does not exist - Decprecated replaced with NamespaceLifecycle controller
6. ...


##### Kubectl ---> Authentication ---> Authorization ---> Admission Controllers ---> Create Resource

##### Let's see all enabled Admission Controllers in our cluster


````

kube-apiserver -h | grep enable-admission-plugins #if your k8s is not using kubeadm and run on a controlplane node

kubectl exec kube-apiserver-controlplane -n kube-system -- kube-apiserver -h | grep enable-admission-plugins

````

##### To add an admission controller modify either the kube-apiserver.service file or /etc/kubernetes/manifests/kube-apiserver.yaml

````
--enable-admision-plugins=NodeRestriction,NamespaceAutoProvision

--disable-admission-plugins=DefaultStorageClass

ps -ef | grep kube-apiserver | grep admission-plugins

````

```Validating & Mutating Admission Controllers```

##### NamespaceExist is a Validating Admission Controller beceause it validates whether or not the namespace exists 

##### DefaultStorageClass is a Mutating Admission Controller beceause it mutates your PVC to add the default storage class, if one exists

##### Mutating comes first then Validating controller


##### k8s supports external admission controllers through 

1. MutatingAdmission Webhook
2. ValidatingAdmission Webhook


##### Admission review object in JSON format

1. Admission Webhook server deployed can be written in any language Go,Python etc.
2. Deploy the server, either within k8s cluster or externally
3. Configure Admission Webhook


````
---
apiVersion: admissionregistration.k8s.io/v1
kind: ValidatingWebhookConfiguration
metadata:
  name: "pod-policy.example.com"
webhooks:
- name:
  clientConfig:
      #url: "https://external-server.example.com"
      service:
        namespace: "webhook-namespace"
        name: "webhook-service"
        caBundle: "Ci0tLS0tQk......tLS0K"
  rules:
   - apiGroups: [""]
     apiVersion: ["v1"]
     operations: ["CREATE"]
     resources: ["pods"]
     scope: "Namespaced"


````

```Pod Security Policy```


````
apiVersion: v1 
kind: Pod
metadata:
  name: sample-pod 

spec:
  containers:
  - name: ubuntu
    image: ubuntu
    command: ["sleep", "3600"]
    securityContext:
      privileged: True 
      runAsUser: 0
      capabilities:
        add: ["CAp_SYS_BOOT"]
  volumes:
  - name: data-volume
    hostPath:
      path: /data 
      type: Directory 


````

##### Enable PSP on the apiserver by either editing the kube-apiserver.service or /etc/kubernetes/manifests/kube-apiserver.yaml


````

--enable-admission-plugins=PodSecurityPolicy

````


```psp.yaml```


````
apiVersion: policy/v1beta1
kind:PodSecurityPolicy
metadata:
  name: example-psp
spec:
  privileged: false
  seLinux:
    rule: RunAsAny
  supplementalGroups:
    rule: RunAsAny
  runAsUser:
    rule: RunAsAny 'MustRunAsNonRoot'
  requiredDropCapabilties:
  - 'CAP_SYS_BOOT'
  defaultAddCapabilties:
  - 'CAP_SYS_TIME'
  volumes:
  - 'persistentVolumeClaim'
  fsGroup:
    rule: RunAsAny

````

##### We must create a role and a rolebinding to authorize access to the apiserver if not all pod creation attempts will fail

##### By default when any prod is created it is assigned the 'default' sa in the namespace so we did to bind that to the role

````
spec:
  serviceAccount: default

````

```psp-example-role.yaml```


````
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: psp-example-role

rules:
- apiGroups: ["policy"]
  resources: ["podsecuritypolicies"]
  resourceNames: ["example-psp"]
  verbs: ["use"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: psp-example-rolebinding 
subjects:
- kind: ServiceAccount
  name: default
  namespace: default
roleRef:
  kind: Role
  name: psp-example-role
  apiGroups: rbac.authorization.k8s.io/v1
````


```OPA```


##### You can download the OPA binary and run it with by default Authentication and Authorization is disabled

````

./opa run -s  #-s means run as server and it listens on port 8181

./opa test . -v #test all my policies in the current directory

````

```example.rego```

````
package httpapi.authz

# HTTP API request
import input

default allow = false

allow {
  input.path == "home"
  inout.user == "john"
}

````

````
curl -X PUT --data-binary @example.rego http://localhost:8181/v1/policies/example1  #load example.rego into opa and call it example1

curl http://localhost:8181/v1/policies #view all policies
````

```app.py```

````
@app.route('/home')
def hello_world():
    user = request.args.get("user")
    if user != "john":
        return 'Unauthorized', 401
    
    return 'Welcome Home!', 200

````

```app-opa.py```


````
@app.route('/home')
def hello_world():
    user = request.args.get("user")
    
    input_dict = {
      "input": {
        "user": user,
        "path": "home",
      }
    }
    rsp = requests.post("http://127.0.0.1:8181/..authz",json=input_dict)
    
    if not rsp.json()["result"]["sllow"]:
        return 'Unauthorized', 401
    
    return 'Welcome Home!', 200

````


```OPA in k8s```


````
apiVersion: admissionregistration.k8s.io/v1beta1
kind: ValidatingWebhookConfiguration
metadata:
  name: opa-validating-webhook

webhooks:
  - name: validating-webhook.openpolicyagent.org
    rules:
      - operations: ["CREATE", "UPDATE"]
        apiGroups: ["*"]
        apiVersions: ["*"]
        resources: ["*"]
    clientConfig:
      #url: "http://opa-address:8181" #externally deployed OPA server
      caBundle: $(cat ca.crt | base64 | tr -d '\n') #opa deployed in cluster
      service:
        namespace: opa
        name: opa

````

###### kube-mgmt agent for OPA

###### With kube-mgmt we can define a configMap with our rego policy 

````
apiVersion: v1
kind: ConfigMap
metadata:
  name: policy-unique-podname 
  namespace: opa
  labels:
    openpolicyagent.org/policy: rego  #this is required

data:
  main: |
    package kubernetes.admission

    import data.kubernetes.pods

    deny[msg]{
      input.request.kind.kind == "Pod"
      input_pod_name := input.request.object.metadata.name
      other_pod_name := pods[other_ns][other_name].metadata.name
      inout_pod_name == other_pod_name
      msg := sprintf("Podname '%v' already exists")
    }

````

```k8s secrets```

````
apiVersion: v1
kind: Secret
metadata:
  name: app-secret

data:
  DB_Host: bqwe==
  DB_User: ased==
  DB_Password: mewq==

````

##### if we mount the secret as a volume we can use the below

````
ls /opt/app-secret-volumes

````

````
apiVersion: v1 
kind: Pod 
metadata: 
  name: envfrom-secret
    
spec:
  containers:
  - name: envars-test-container
    image: nginx
    envFrom:
    - secretRef:
        name: test-secret

````

[https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/#define-container-environment-variables-using-secret-data](https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/#define-container-environment-variables-using-secret-data)


```Encrypt Secret Data @ Rest```

[https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)


##### volumeMounts is what get's mounted in the pod itself, volumes is what will be placed in the volumeMounts


````

crictl pods


kubectl get secrets --all-namespaces -o json | kubectl replace -f -  ##get all secrets and then re-apply the same config so they now get encrypted at rest

````


```Sandboxing```



```gVisor```


##### Additional layer of isolation between container and the kernel

1. Sentry - Intercept system calls from containers
2. Gofer - file proxy Sentry calls Gofer


```Kata Containers```

##### Kata creates a micro VM with it's own kernel and deploys the a container in it for isolation


```Runtime Classes```

##### runc actually runs the container

##### Kata containers uses kata-runtime and gVisor uses Runsc these are all OCI compliant


````
docker run --runtime kata -d nginx #use kata runtime

docker run --runtime runsc -d nginx  #use gVisor runtime

````


```Installing different runtimes in k8s```


```RuntimeClass```


````
apiVersion: node.k8s.io/v1  #for newer clusters use this version
kind: RuntimeClass
metadata:
  name: gvisor 
handler: runsc

````

```pod-spec```

````
apiVersions: v1
kind: Pod
metadata:
  name: nginx
  labels:
    run: nginx

spec:
  runtimeClassName: gvisor
  containers:
  - image: nginx
    name: nginx

````

##### Check to make sure that the above is now in it's own micro-kernel VM

````
pgrep -a nginx
pgrep -a runsc #run both command on the node that the pod is running on

````

```One Way SSL/TLS vs Mutual SSL/TLS```


```mTLS```

##### use a Service Mesh Isitio or Linkerd

###### Istio modes

1. Enforce/Strict mTLS
2. Permissive/Opportunistic


[https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)

[https://kubernetes.io/docs/concepts/configuration/secret/#protections](https://kubernetes.io/docs/concepts/configuration/secret/#protections)

[https://kubernetes.io/docs/concepts/configuration/secret/#risks](https://kubernetes.io/docs/concepts/configuration/secret/#risks)

[https://kubernetes.io/docs/concepts/configuration/secret/#risks](https://kubernetes.io/docs/concepts/configuration/secret/#risks)


```Minimize Base Image```

1. Do not build image with multiple services, build modular images i.e. do not add a web-server, db etc into 1 image
2. Persist State, do not store state in a container, so persist state to either cache,db,pv 
3. Choose official images where possible, and also choose images that are frequently updated, again where possible
4. Create slim/minimal images for your base image
5. Delete and cleanup as much as possible from the image
6. Prod images remove shells, package managers and any other tool your app does not need to run
7. Use multi-stage builds so your build tools does not come with your run image 
8. Use distro-less images where possible


```Image Security```

##### when you say image: nginx you are actually saying library/nginx library is the account where Docker stores all official images and

##### and the image is stored at docker.io so the full name of nginx is image: docker.io/library/nginx


````
kubectl create secret docker-registry regcred \
--docker-server=harbor.fbclouddemo.us \
--docker-username=myuser \
--docker-password=my_password \
--docker-email=myuser@me.com

````

##### Add imagePullSecret to existing deployment

````
spec:
      containers:
      - image: myprivateregistry.com:5000/nginx:alpine
        imagePullPolicy: IfNotPresent
        name: nginx
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      imagePullSecrets:
      - name: private-reg-cred

````

```Whitelist allowed registry```

##### ImagePolicyWebhook Admission Controller

##### /etc/kubernetes/admission-config.yaml


````
apiVersion: apiserver.config.k8s.io/v1
kind: AdmissonConfiguration
plugins:
- name: ImagePolicyWebhook
  configuration:
    imagePolicy:
      kubeConfigFile: <Path-to-kubeconfig-file>
      allowTTL: 50
      denyTTL: 50
      retryBackoff: 500
      defaultAllow: true

````

````
clusters:
- name: name-of-remote-image-policy-server
  cluster:
    certificate-authority: /path/to/ca.pem
    server: https://images.example.com/policy

users:
- name: name-of-api-server
  user:
    client-certificate: /path/to/cert.pem
    client-key: /path/to/key.pem
````

````
--enable-admission-plugin=ImagePolicyWebhook #if kube-api is a service on the machine
--admission-control-config-file=/etc/kubernetes/admission-config.yaml

# What worked was the below
--enable-admission-plugins=NodeRestriction,ImagePolicyWebhook
--admission-control-config-file=/etc/kubernetes/admission-config.yaml

##/etc/kubernetes/manifests/kube-apiserver.yaml

- --enable-admission-plugins=ImagePolicyWebhook
- --admission-control-config-file=/etc/kubernetes/admission-config.yaml

# What worked was the below
- --enable-admission-plugins=NodeRestriction,ImagePolicyWebhook
- --admission-control-config-file=/etc/kubernetes/admission-config.yaml
````

```Use Static Analysis```

[https://kubesec.io](https://kubesec.io)


##### You can use kubesec in a few ways 

1. Download Binary(I Don't see a Windows version)
2. curl -sSX POST --data-binary @"pod.yaml" https://v2.kubesec.io/scan
3. kubesec http 8080 &(run a local web server)


````
kubesec scan /root/node.yaml -f json -o /root/kubesec_report.json

````

```Scan container images```

##### CVE - Common Vulnerability and Exposures

[https://trivy.dev/](https://trivy.dev/)

````
trivy image nginx:1.18.0

trivy image --severity CRITICAL nginx:1.18.0

trivy image --severity CRITICAL,HIGH nginx:1.18.0

trivy image --ignore-unfixed nginx:1.18.0

docker save nginx:1.18.0 > nginx.tar

trivy image --input nginx.tar

````

##### Best Practices

1. Continuosly rescan images
2. Kubernetes Admission Controllers to scan images
3. Have your own repository with pre-scanned images ready to go
4. Integrate scanning into your CI/CD pipeline



````
trivy image --input /root/alpine.tar --format json -o /root/alpine.json

````

```Perform Behavorial analytics```

##### Falco is a tool we can use, Falco looks at user-space and kernel-space, it uses a kernel module, it can also use eBPF

##### It can be installed as a service on the nodes, or as a DS in k8s

````
systemctl status falco

journalctl -fu falco

````


```rules.yaml```

````
- rule: Detect shell inside a container
  desc: Alert if shell such as bash is opened in a container
  #condition: container.id != host and proc.name = bash
  #condition: container.id != host and proc.name in (linux_shells)
  condition: container != host and proc.name in (linux_shells)
  output: Bash shell opened (user=%user.name %container.id)
  priority: WARNING

- list: linux_shells
  items: [bash,zsh,ash,ksh,sh,csh]
  
- macro:container 
  condition: container.id
````

1. container.id 
2. proc.name 
3. fd.name 
4. evt.type 
5. user.name 
6. container.image.repository


##### Priority can be one of 

1. DEBUG 
2. INFORMATIONAL
3. NOTICE 
4. WARNING 
5. ERROR 
6. CRITICAL 
7. ALERT 
8. EMERGENCY


##### main falco file /etc/falco/falco.yaml


```/etc/falco/falco.yaml```


````
stdout_output:
  enabled: true

file_output:
  enabled: true
  filename: /opt/falco/events.txt 

program_output:
  enabled_true:
  program: "jq | '{text: .output}' | curl -d @- -X POST https://hooks.slack.com/services/XXX"

http_output:
  enabled: true
  url: http://some.url/some/path/
````

##### /etc/falco/falco_rules.yaml is where the default rules live

##### /etc/falco/falco_rules.local.yaml make all rule changes here and even add your own custom rules


```hot reload```

````
cat /var/run/falco.pid 

kill -1 $(cat /var/run/falco.pid)
````

```Mutable vs immutable infra```

##### Mutable - mutate

##### Immutable - on-changed


```Mutable container```


````
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    securityContext:
      readOnlyRootFilesystem: true 
    volumeMounts:
      - name: cache-volume
        mountPath: /var/cache/nginx 
      - name: runtime-volume
        mountPath: /var/run
  volumes:
    - name: cache-volume
      emptyDir: {}
    - name: runtime-volume
      emptyDir: {}
    ports:
    - containerPort: 80

````

##### Also enforce that we don't use privilege containers

```Audit Logs```


1. RequestReceived
2. ResponseStarted
3. ResponseComplete
4. Panic - if error


```audit-policy.yaml```


````
apiVersion: audit.k8s.io/v1  #as of k8s 1.20
kind: Policy
omitStages: ["RequestReceived"] #this is optional
#omitStages:
  #- RequestReceived #we can rewrite the above like this
rules:
  - namespaces: ["prod-namespace"] #optional if not there all namespaces are checked *
    verbs: ["delete"] #also optional, again if not their all verbs are audited
    resources:
    - groups: " "
      resources: ["pods"]
      resourceNames: ["webapp-pod"] #Optional this is matching the exact pod name
    level: RequestResponse #most verbose
  
  - level: Metadata #least verbosity
    resources:
    - groups: " "
      resources: ["secrets"]
  
 
````

##### To wire this up we need to update the api-server static pod located at /etc/kubernetes/manifests/kube-apiserver.yaml

````
- --audit-log-path=/var/log/k8-audit.log
- --audit-policy-file=/etc/kubernetes/audit-policy.yaml
- --audit-log-maxage=10
- --audit-log-maxbackup=5
- --audit-log-maxsize=100

````


````
apiVersion: audit.k8s.io/v1  #as of k8s 1.20
kind: Policy

rules:

- level: Metadata #least verbosity
  namespaces: ["prod"]
  verbs: ["delete"]
  resources:
  - groups: ""
    resources: ["secrets"]

````
